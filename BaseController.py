__author__ = 'raytlty'
#-*- coding: UTF-8 -*-

import os
import json
import redis
import tornado.options
import tornado.web
import tornado.escape
from Model.BaseModel import BaseModel
from tornado.options import define, options


class BaseHandler(tornado.web.RequestHandler):

    # authorize and get the userId
    def isAuthorized(self, token):
        baseModel = BaseModel()
        if(baseModel.redisconn.exists(token)):
            return baseModel.redisconn.get(token)
        else:
            raise tornado.web.HTTPError(401)

    def MsgResponse(self, **kwargs):
        '''
            response func
        '''
        self.set_header('Content-type','application/json')
        # print(kwargs)
        self.finish(tornado.escape.json_encode(kwargs))

    @property
    def AddressModel(self):
        return self.settings['AddressModel']

    @property
    def OrderModel(self):
        return self.settings['OrderModel']

    @property
    def AdsModel(self):
        return self.settings['AdsModel']

    @property
    def ShopModel(self):
        return self.settings['ShopModel']

    @property
    def SMSModel(self):
        return self.settings['SMSModel']


    @property
    def SuperManagerModel(self):
        return  self.settings['SuperManagerModel']

    @property
    def CheckStudentAccount(self):
        return self.settings['CheckStudentAccount']

    @property
    def StudentModel(self):
        return self.settings['StudentModel']

    @property
    def PrintTaskModel(self):
        return self.settings['PrintTaskModel']

    @property
    def rootPath(self):
        return self.settings['root_path']

    @property
    def userFile(self):
        file_path = self.settings['static_path'] + os.sep + 'files'
        return file_path




if __name__ == '__main__':
    pass