# -*- coding: utf-8 -*-
__author__ = 'raytlty'

from BaseController import BaseHandler
from tornado.escape import json_decode
from tornado.web import MissingArgumentError
import json
import os
from Utils.Utils import FuckThePy2

# shop/smsCode
class ShopSMS(BaseHandler):
    def post(self, *args, **kwargs):
        SMSModel = self.SMSModel
        try:
            username = self.get_body_argument('username')
        except MissingArgumentError:
            body_data = json_decode(self.request.body.decode('utf-8'))
            body_data = FuckThePy2(body_data)
            username = body_data.get('username')
        data = SMSModel.sendTemplateSMS(username)
        self.MsgResponse(**data)

# shop/signup
class ShopSignup(BaseHandler):
    def post(self, *args, **kwargs):
        ShopModel = self.ShopModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        self.MsgResponse(**ShopModel.ShopsignUp(**body_data))

# shop/info
class ShopCompleteInfo(BaseHandler):
    def post(self, *args, **kwargs):
        ShopModel = self.ShopModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        shopid = self.isAuthorized(token).decode('utf-8')
        body_data['shopid'] = int(shopid)
        self.MsgResponse(**ShopModel.completeShopInfo(**body_data))

# shop/password
class ShopForgetPassword(BaseHandler):
    def post(self, *args, **kwargs):
        ShopModel = self.ShopModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        # token = body_data.get('token')
        # shopid = self.isAuthorized(token).decode('utf-8')
        # body_data['shopid'] = int(shopid)
        self.MsgResponse(**ShopModel.ShopForgetPassword(**body_data))

# shop/login
class ShopLogin(BaseHandler):
    def post(self, *args, **kwargs):
        ShopModel = self.ShopModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        self.MsgResponse(**ShopModel.Shoplogin(**body_data))

# shop/logout
class ShopLogout(BaseHandler):
    def post(self, *args, **kwargs):
        ShopModel = self.ShopModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        shopid = self.isAuthorized(token).decode('utf-8')
        body_data['shopid'] = int(shopid)
        self.MsgResponse(**ShopModel.Shoplogout(**body_data))

# order/unprinted
class ShopOrderUnPrinted(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        shopid = self.isAuthorized(token).decode('utf-8')
        body_data['shopid'] = int(shopid)
        self.MsgResponse(**OrderModel.GetUnprintOrder(**body_data))

# order/uncopy
class ShopOrderUnCopy(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        shopid = self.isAuthorized(token).decode('utf-8')
        body_data['shopid'] = int(shopid)
        self.MsgResponse(**OrderModel.GetUnCopyOrder(**body_data))

# order/comfirmed
class ShopConfirmOrder(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        shopid = self.isAuthorized(token).decode('utf-8')
        body_data['shopid'] = int(shopid)
        self.MsgResponse(**OrderModel.ConfirmedShopOrder(**body_data))


# order/printed/details
class ShopPrintedDetails(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        shopid = self.isAuthorized(token).decode('utf-8')
        body_data['shopid'] = int(shopid)
        self.MsgResponse(**OrderModel.UnfinishDetails(**body_data))

# order/copy/details
class ShopCopyDetails(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        shopid = self.isAuthorized(token).decode('utf-8')
        body_data['shopid'] = int(shopid)
        self.MsgResponse(**OrderModel.UnfinishDetails(**body_data))


# order/history
class ShopHistoryOrder(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        shopid = self.isAuthorized(token).decode('utf-8')
        body_data['shopid'] = shopid
        self.MsgResponse(**OrderModel.HistoryOrder(**body_data))

# /v1/order/download
class DownloadOrder(BaseHandler):
    def get(self, *args, **kwargs):
        shop_id = int(self.isAuthorized(self.get_argument('token')))
        order_id =int(self.get_argument('order_id'))
        student_id = int(self.get_argument('student_id'))
        OrderModel = self.OrderModel
        data = dict(
            student_id = student_id,
            order_id = order_id
        )
        zipfile_name, zipfile_url = OrderModel.download_url(**data)
        self.set_header('Content-Type', 'application/x-zip-compressed')
        self.set_header('Content-Disposition', 'attachment;filename=' + zipfile_name.encode('utf-8'))
        buf_size = 1024
        with open(zipfile_url.encode('utf-8'), 'rb') as zfile:
            while True:
                data = zfile.read(buf_size)
                if not data:
                    break
                self.write(data)
        self.finish()