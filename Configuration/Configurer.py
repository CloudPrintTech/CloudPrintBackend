# -*- coding: utf-8 -*-

import sys

__author__ = 'Shuyu'


def singleton(cls, *args, **kw):
    instances = {}
    def _singleton():
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]
    return _singleton

@singleton
class Configurer(object):
    def __init__(self):

        if len(sys.argv) > 2:
            self.port = sys.argv[1]
            self.env  = sys.argv[2]
        else:
            self.port = 2368
            self.env  = 'dev'

        self.mysql_url = 'mysql+pymysql://test:Playstation2@localhost/cloudprinter?charset=utf8'
        self.redis_url = 'localhost'
        self.oss_end_point = 'oss-cn-shanghai.aliyuncs.com'

        if self.env == 'dev':
            self.base_url  = 'http://api.in.uprintf.com'
        elif self.env == 'master':
            self.base_url  = 'http://api.uprintf.com'
            self.oss_end_point = 'oss-cn-shanghai-internal.aliyuncs.com'
        else:
            self.base_url  = 'http://api.in.uprintf.com'
            self.mysql_url = 'mysql+pymysql://test:Playstation2@in.uprintf.com/cloudprinter?charset=utf8'
