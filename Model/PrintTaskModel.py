# -*- coding: utf-8 -*-
__author__ = 'Arthur'
from Model.BaseModel import BaseModel
from Model.ORM import *
import os
from sqlalchemy import and_
import time
from pyPdf import PdfFileWriter, PdfFileReader
import uuid
import urllib
import hmac
import base64
import sha
# import win32com
# from win32com.client import Dispatch,constants


class PrintTaskModel(BaseModel):
    def isExisted(self,file_key):
        session = self.session
        return session.query(PrintTask).filter(PrintTask.file_key==file_key).first()

    def beginCreate(self,OSSAccessKeyId,file_key):
        now = int(time.time())+60
        objectName = str(uuid.uuid1())
        h = hmac.new("a9Zeg5f39598ASF8cGjNl0oOyWjdjD",
             "PUT\n\n\n"+str(now)+"\nstudent-documents/"+file_key+"\n", sha)
        signature = urllib.quote_plus(base64.encodestring(h.digest()).strip())
        url =  "http://student-documents.oss-cn-shanghai.aliyuncs.com/object_name1?Expires="+str(now)+"&OSSAccessKeyId="+OSSAccessKeyId+"&Signature="+signature
        return self.success([{"url":url,"file_id":objectName}])

    def uploadTask(self,student_id,file_name,file_key,file_url,pages):
        try:
            session = self.session
            task = PrintTask(student_id=student_id,file_name=file_name,file_key=file_key,file_url=file_url,pages=pages)
            session.add(task)
            session.commit()
            return self.success([{'task_id':task.task_id,"pages":pages}],"上传文件成功")
        except Exception as e:
            return self.err(e)

    def uploadTaskV2(self,student_id,file_name,file_key,file_url,pages):
        try:
            session = self.session
            task = PrintTask(student_id=student_id,file_name=file_name,file_key=file_key,file_url=file_url,pages=pages)
            session.add(task)
            session.commit()
            return task.task_id
            # return self.success([{'task_id':task.task_id,"pages":pages}],"上传文件成功")
        except Exception as e:
            return False
            # return self.err(e)

    def deleteTask(self,task_id):
        try:
            sess = self.session
            nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
            task = sess.query(PrintTask).filter(PrintTask.task_id==task_id).update({"deleted_at":nowtime,"task_state":-1})
            sess.commit()
            return self.success([],"删除任务成功")
        except Exception,e:
            return self.err(e)


    def getTaskPages(self,task_id,root_path):
        try:
            session = self.session
            task = session.query(PrintTask.file_url,PrintTask.pages).filter(PrintTask.task_id==task_id).one()
            if task.pages:
                pages = task.pages
            else:
                doc = PdfFileReader(file(task.file_url, "rb"))
                pages = doc.getNumPages()
                session.query(PrintTask).filter(PrintTask.task_id==task_id).update({"pages":pages})
                session.commit()
            return self.success([{'pages':pages},"获取页数成功"])
        except Exception,e:
            return self.err(e)

    def uploadTasks(self,student_id,files,userFile):
        try:
            task = list()
            for f in files:
                file_name = f['filename']
                filepath = os.path.join(userFile,student_id.decode('utf-8')+os.sep+str(int(time.time())))
                if not os.path.exists(filepath):
                    os.makedirs(filepath)
                file_path = os.path.join(filepath, file_name)
                with open(file_path, 'wb') as up:
                    up.write(f['body'])     # 写入文件
                    task.append(PrintTask(student_id=student_id,file_name=file_name,file_url=file_path))
            session = self.session
            session.add(task)
            session.commit()
            tasks = list()
            unprintedTasks = session.query(PrintTask.task_id,PrintTask.file_name).filter(and_(PrintTask.student_id==student_id,PrintTask.order_id == None)).all()
            for item in unprintedTasks:
                tasks.append(dict(zip(["task_id","file_name"],item)))
            results = list()
            results.append({'todoTasks': tasks})
            return self.success(results,"文件上传成功")
        except Exception as msg:
            return self.err(msg)

    def createTask(self,**kwargs):
        try:
            session = self.session
            printTask = PrintTask(**kwargs)
            session.add(printTask)
            session.commit()
            return self.success([{'task_id':printTask.task_id}],"创建复印任务成功")
        except Exception, e:
            return self.err(e)

if __name__ == "__main__":
    p = PrintTaskModel()



