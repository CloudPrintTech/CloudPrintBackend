# -*- coding: utf-8 -*-
__author__ = 'raytlty'


import os
import time
import base64
import hashlib
from Model.BaseModel import BaseModel
from Model.ORM import *
from sqlalchemy.sql import exists
from sqlalchemy.sql import and_
import logging
import tornado.web

class ShopModel(BaseModel):

    def updateInfo(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            managerid = kwargs['managerid']
            school = kwargs['school']
            building = kwargs['building']
            manage_buildings = kwargs.get('manage_buildings',[])
            logging.info(building)
            logging.info(school)
            data = dict(
                manager_name = kwargs['manager_name'],
                school_id = school['ID'],
                qq = kwargs['qq'],
                sex = kwargs['sex'],
                email = kwargs['email'],
                graduate_year = kwargs['graduate_year'],
                alipay_account = kwargs['alipay_account'],
                dorm = kwargs['dorm'],
                building_id = building['ID'],
                ID_number = kwargs['ID_number'],
                 #已完善信息
            )

            session.query(ShopManager).filter(ShopManager.manager_id == managerid).update(values=data,synchronize_session=False)
            items = session.query(Building).filter(Building.deliver_id == managerid).all()
            session.flush()
            if items:
                for item in items:
                    building_id = item.id
                    session.query(Building).filter(Building.id == building_id).update(dict(deliver_id=None))
                    session.flush()
            if manage_buildings:
                    for item in manage_buildings:
                        buildingID, buildingName, buildingStatus= item['ID'], item['name'], item['status']
                        session.query(Building).filter(and_(Building.id == buildingID,
                                                            Building.name == buildingName,
                                                            Building.status == buildingStatus)).update(dict(deliver_id=managerid),synchronize_session=False)
                        session.flush()
            session.commit()
            return self.success(msg='更新成功')
        except KeyError :
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def completeInfo(self, **kwargs):
        session = self.session
        try:
            #获取了managerid之后就以managerid命名
            managerid = kwargs['managerid']

            # 原为保存学生证和身份证，但现在不需要保存，只需要存base64串
            # studentCard = kwargs['certificate']
            # studentCard_decode = base64.b64decode(studentCard)
            # IDCard = kwargs['ID_card']
            # IDCard_decode = base64.b64decode(IDCard)
            # filePath = kwargs.get('filePath')
            # if not os.path.exists(filePath):
            #     os.makedirs(filePath)

            school = kwargs['school']
            building = kwargs['building']
            manage_buildings = kwargs.get('manage_buildings',[])
            data = dict(
                manager_name = kwargs['manager_name'],
                school_id = school['ID'],
                qq = kwargs['qq'],
                sex = kwargs['sex'],
                email = kwargs['email'],
                graduate_year = kwargs['graduate_year'],
                alipay_account = kwargs['alipay_account'] ,
                certificate = kwargs['certificate'],
                dorm = kwargs['dorm'],
                ID_number = kwargs['ID_number'],
                building_id = building['ID'],
                state = 1 #已完善信息
            )

        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(emsg)
        else:
            try:
                if manage_buildings:
                    for item in manage_buildings:
                        buildingID, buildingName, buildingStatus= item['ID'], item['name'], item['status']
                        session.query(Building).filter(and_(Building.id == buildingID,
                                                            Building.name == buildingName,
                                                            Building.status == buildingStatus)).update(dict(deliver_id=managerid),synchronize_session=False)
                        session.flush()
                session.query(ShopManager).filter(ShopManager.manager_id == managerid).update(values=data,synchronize_session=False)
                session.commit()

                # 原为保存学生证和身份证，但现在不需要保存，只需要存base64串
                # ptr = open(os.path.join(filePath, 'studentCard.png'), 'wb')
                # try:
                #     ptr.write(studentCard_decode)
                # except Exception as emsg:
                #     return self.err(msg=emsg)
                # finally:
                #     ptr.close()
                # ptr = open(os.path.join(filePath, 'IDCard.png'), 'wb')
                # try:
                #     ptr.write(IDCard_decode)
                # except Exception as emsg:
                #     return self.err(msg=emsg)
                # finally:
                #     ptr.close()

            except Exception as emsg:
                return self.err(emsg)
            else:
                return self.success([], '完善个人信息成功')

    def signUp(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            username = kwargs['username']
            password = kwargs['password']
            smsCode = kwargs['smsCode']
            smanager = session.query(ShopManager).filter(ShopManager.username == username).first()

            if smanager != None:
                return self.err("此手机号已注册，请登录")

            if not redisconn.exists(username):
                return self.err("验证码已过期，请重新请求发送")

            if redisconn.get(username)!=smsCode.encode('utf8'):
                return self.err( "输入的验证码错误")
            password = hashlib.md5((password+':'+username).encode()).hexdigest()

            shopmanager = ShopManager(username=username,password=password)
            session.add(shopmanager)
            session.commit()
            token = hashlib.md5((username+':'+str(time.time())+smsCode).encode()).hexdigest()

            manager_id= session.query(ShopManager.manager_id)\
                                            .filter(ShopManager.username == username).first()[0]
            redisconn.set(token,manager_id)
        except Exception as emsg:
            return self.err(str(emsg))
        else:
            return self.success([{'token':token}],"注册成功，请完善信息")

    def login(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            username = kwargs['username']
            password = kwargs['password']
            device_token = kwargs['device_token']

            smanager = session.query(ShopManager).filter(ShopManager.username == username).first()
            if smanager == None:
                return self.err('请用手机号注册账号')
            else :
                if smanager.state == 3:
                    return self.err(msg='您已经辞职')

                realpasswd, manager_id, state = smanager.password, smanager.manager_id, smanager.state

                testpasswd = hashlib.md5((password+':'+username).encode()).hexdigest()
                token = hashlib.md5((username + ':' +str(time.time()) + realpasswd).encode()).hexdigest()
                week = 60 * 60 * 24 * 7
                redisconn.setex(token, manager_id, week)
                redisconn.setex(manager_id, device_token, week)
                results = []
                if realpasswd == testpasswd:
                    results.append({'token':token, 'state':state})
                    return self.success(results=results, msg='登录成功')
                else :
                    return self.err(msg='密码错误')

        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def logout(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            managerid = kwargs['managerid']
            device_token = redisconn.get(managerid)
            redisconn.delete(token)
            redisconn.delete(device_token)
            return self.success()
        except Exception as emsg:
            return self.err(msg=emsg)
        except KeyError:
            raise tornado.web.HTTPError(400)

    def forgetPassword(self,**kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            username = kwargs['username']
            password = kwargs['password']
            smsCode = kwargs['smsCode']

            if not redisconn.exists(username):
                return self.err("验证码已过期，请重新请求发送")
            if redisconn.get(username)!=smsCode:
                return self.err( "输入的验证码错误")

            realpassword = hashlib.md5((str(password) + ':' + username).encode()).hexdigest()
            token = hashlib.md5((username + ':' + str(time.time()) + realpassword).encode()).hexdigest()
            shopManager = session.query(ShopManager).filter(ShopManager.username == username).first()
            if shopManager is None:
                return self.err(msg='用手机号注册')
            week = 60 * 60 * 24 * 7
            redisconn.setex(token, shopManager.manager_id, week)
            session.query(ShopManager).filter(ShopManager.username == username).update(dict(password=realpassword),synchronize_session=False)
            session.commit()
        except Exception as emsg:
            return self.err(msg=emsg)
        else:
            return self.success([{'token':token}],'修改密码成功')

    def getMyInfo(self,**kwargs):
        session = self.session
        redconn = self.redisconn
        try:
            token = kwargs['token']
            managerid = kwargs['managerid']

            item = session.query(ShopManager).filter(ShopManager.manager_id == managerid).first()
            if item is None:
                return self.err(msg='请用手机号登陆')
            def getAddress(school_id, building_id):
                if not school_id and not building_id:
                    return {'ID':school_id, 'name':None}, {'ID':building_id, 'name':None, 'status':None}
                elif not school_id:
                    return {'ID':school_id, 'name':None}, {'ID':building_id, 'name':None, 'status':None}
                name = session.query(Community.name).filter(Community.id == school_id).first().name
                buildName, status = session.query(Building.name, Building.status).filter(Building.id == building_id).first()
                return {'ID':school_id, 'name':name}, {'ID':building_id, 'name':buildName, 'status':status}

            def getManageBuilding(managerid):
                res = []
                items = session.query(Building).filter(Building.deliver_id == managerid).all()
                if items:
                    for item in items:
                        data = dict(
                            ID = item.id,
                            name = item.name,
                            status = item.status,
                        )
                        res.append(data)
                return res

            school, building= getAddress(school_id=item.school_id, building_id=item.building_id)
            manage_buildings = getManageBuilding(managerid)
            results = [
                    dict(
                        manager_name = item.manager_name,
                        qq = item.qq,
                        sex = item.sex,
                        email = item.email,
                        graduate_year = item.graduate_year,
                        alipay_account = item.alipay_account,
                        ID_number = item.ID_number,
                        dorm = item.dorm,
                        state = item.state,
                        school = school,
                        building = building,
                        manage_buildings = manage_buildings,
                    )
            ]
            return self.success(results=results)
        except KeyError :
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def changePassword(self,**kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            managerid = kwargs['managerid']
            oldpassword = kwargs['oldPassword']
            newpassword = kwargs['newPassword']

            item= session.query(ShopManager).filter(ShopManager.manager_id == managerid).first()
            if item is None:
                return self.err(msg='请重新登录')
            username = item.username
            passwd = item.password

            testpasswd = hashlib.md5((str(oldpassword) + ':' + username).encode()).hexdigest()
            if testpasswd != passwd:
                return self.err(msg='输入原密码错误')
            realpasswd = hashlib.md5((str(newpassword) + ':' + username).encode()).hexdigest()
            session.query(ShopManager).filter(ShopManager.manager_id == managerid).update(dict(password=realpasswd),synchronize_session=False)
            session.commit()
            token = hashlib.md5((username + ':' + str(time.time()) + realpasswd).encode()).hexdigest()
            week = 60 * 60 * 24 * 7
            redisconn.setex(token, managerid, week)
            return self.success(msg='修改成功')
        except KeyError :
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def resign(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            mananerid = kwargs['managerid']
            token = kwargs['token']
            state = 3 #已辞职
            session.query(ShopManager).filter(ShopManager.manager_id == mananerid).update(dict(state=state))
            session.flush()
            session.query(Building).filter(Building.deliver_id == mananerid).update(dict(deliver_id=None), synchronize_session=False)
            session.commit()
            redisconn.delete(token)
            device_token = redisconn.get(mananerid)
            redisconn.delete(device_token)
            return self.success()
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)


    def getComplain(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            mananerid = kwargs['managerid']
            token = kwargs['token']
            lastid = kwargs.get('lastid', 0)

            items = session.query(PrintOrder).order_by(PrintOrder.order_id.desc())
            if lastid != 0:
                items = items.filter(and_(PrintOrder.manager_id == mananerid,
                                                          PrintOrder.order_id < lastid,
                                                          PrintOrder.complain != None)).limit(20).all()
            else:
                items = items.filter(and_(PrintOrder.manager_id == mananerid,
                                                          PrintOrder.complain != None)).limit(20).all()
            results = []
            if items is None:
                return self.success(results=results, msg='恭喜你~没有投诉信息')
            for item in items:
                data = dict(
                    ID = item.order_id,
                    trade_no = item.trade_no,
                    complain_at = int(time.mktime(time.strptime(item.complain_at.strftime('%Y-%m-%d %H:%M:%S'),'%Y-%m-%d %H:%M:%S'))),
                    complain = item.complain,
                )
                results.append(data)
            return self.success(results=results, msg='获取成功')
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

##########################################
#打印店

    def ShopsignUp(self,**kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            username = kwargs['username']
            password = kwargs['password']
            smsCode = kwargs['smsCode']
            shopitem = session.query(Shop).filter(Shop.username == username).first()

            if shopitem != None:
                return self.err("此手机号已注册，请登录")

            if not redisconn.exists(username):
                return self.err("验证码已过期，请重新请求发送")

            if redisconn.get(username)!=smsCode.encode('utf8'):
                return self.err( "输入的验证码错误")
            password = hashlib.md5((password+':'+username).encode()).hexdigest()

            shop = Shop(username=username,password=password)
            session.add(shop)
            session.commit()
            token = hashlib.md5((username+':'+str(time.time())+smsCode).encode()).hexdigest()

            shop_id= session.query(Shop.id)\
                                            .filter(Shop.username == username).first()[0]
            redisconn.set(token,shop_id)
            return self.success(results=[{'token':token}],msg='注册成功，请登陆完善信息')
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def Shoplogin(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            username = kwargs['username']
            password = kwargs['password']

            shopitem = session.query(Shop).filter(Shop.username == username).first()
            if shopitem == None:
                return self.err('请用手机号注册账号')
            else :
                realpasswd, shop_id, state = shopitem.password, shopitem.id, shopitem.state

                testpasswd = hashlib.md5((password+':'+username).encode()).hexdigest()
                token = hashlib.md5((username + ':' +str(time.time()) + realpasswd).encode()).hexdigest()
                week = 60 * 60 * 24 * 7
                redisconn.setex(token, shop_id,week)
                results = []
                if realpasswd == testpasswd:
                    results.append({'token':token, 'state':state})
                    return self.success(results=results, msg='登录成功')
                else :
                    return self.err(msg='密码错误')
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def completeShopInfo(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            shopid = kwargs['shopid']
            school = kwargs['school']
            del kwargs['token'], kwargs['shopid'], kwargs['school']
            kwargs['community_id'] = school['ID']
            session.query(Shop).filter(Shop.id == shopid).update(values=kwargs,synchronize_session=False)
            session.commit()
            return self.success()
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def Shoplogout(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            shopid = kwargs['shopid']
            redisconn.delete(token)
            return self.success()
        except Exception as emsg:
            return self.err(msg=emsg)
        except KeyError:
            raise tornado.web.HTTPError(400)

    def ShopForgetPassword(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            username = kwargs['username']
            password = kwargs['password']
            smsCode = kwargs['smsCode']
            shopitem = session.query(Shop).filter(Shop.username == username).first()
            if shopitem == None:
                return self.err(msg='请用手机号注册')

            if not redisconn.exists(username):
                return self.err("验证码已过期，请重新请求发送")

            if redisconn.get(username)!=smsCode.encode('utf8'):
                return self.err( "输入的验证码错误")

            shop_id = shopitem.id
            password = hashlib.md5((password+':'+username).encode()).hexdigest()

            shop = Shop(id=shop_id,password=password)
            session.merge(shop)
            session.commit()
            token = hashlib.md5((username+':'+str(time.time())+smsCode).encode()).hexdigest()
            redisconn.set(token,shop_id)
            return self.success(results=[{'token':token}],msg='修改密码成功')
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)


if __name__ == '__main__':
    shopModel = ShopModel()
    args = dict(
        managerid = 2,
    )
    shopModel.getMyInfo(**args)
