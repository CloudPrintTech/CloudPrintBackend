# -*- coding: utf-8 -*-
__author__ = 'raytlty'


from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,ForeignKey
from sqlalchemy.types import VARCHAR,CHAR
from sqlalchemy.dialects.mysql import INTEGER as Integer, TIME as Time, TINYINT as tinyInt, FLOAT as Float,TEXT
from sqlalchemy.dialects.mysql import TIMESTAMP as TimeStamp
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.elements import Null
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine
import logging

Base = declarative_base()


def func(self, **kwargs):
    for key, value in kwargs.items():
        instance = getattr(self, key)
        if instance == None and value !=None:
            setattr(self,key,value)

class Test(object):
    def __init__(self, val1=None, val2=None):
        self.val1 = val1
        self.val2 = val2


class BaseModelORM(Base):
    __abstract__ = True
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }

class City(BaseModelORM):
    __tablename__ = 'city'
    id = Column(Integer(display_width=10, unsigned=True), autoincrement=True,nullable=False, primary_key=True)
    name = Column(VARCHAR(255), nullable=False)
    alpha = Column(VARCHAR(255))
    firstalpha = Column(VARCHAR(255))
    attr = Column(VARCHAR(255))
    status = Column(tinyInt(display_width=4))
    addtime = Column(Integer(display_width=11))
    modtime = Column(Integer(display_width=11))
    provinceid = Column(Integer(display_width=10, unsigned=True), nullable=False)
    #one to many relationship: one city has many communities
    # communities = relationship("Community",backref('City'))



class Community(BaseModelORM):
    __tablename__  = 'community'
    id = Column(Integer(display_width=10, unsigned=True), nullable=False, primary_key=True, autoincrement=True)
    name = Column(VARCHAR(255))
    nickname = Column(VARCHAR(255))
    cityid = Column(Integer(unsigned=True), ForeignKey('city.id'), nullable=False)

    # a community has many buildings
    # buildings = relationship('Building',backref('Community'))

    maxlat = Column(Float)
    minlat = Column(Float)
    maxlng = Column(Float)
    minlng = Column(Float)
    attr = Column(VARCHAR(255))
    status = Column(tinyInt(display_width=4))
    addtime = Column(Integer(display_width=11))
    modtime = Column(Integer(display_width=11))


class Academy(BaseModelORM):
    __tablename__ = 'academy'
    id = Column(Integer(unsigned=True),nullable=False,autoincrement=True,primary_key=True)
    community_id = Column(Integer(unsigned=True),ForeignKey('community.id'),nullable=False)
    year = Column(CHAR(4),nullable=False)
    name = Column(VARCHAR(255),nullable=False)

class Major(BaseModelORM):
    __tablename__ = 'major'
    id = Column(Integer(unsigned=True),nullable=False,autoincrement=True,primary_key=True)
    academy_id = Column(Integer(unsigned=True),ForeignKey('academy.id'),nullable=False)
    name = Column(VARCHAR(255),nullable=False)

class Class(BaseModelORM):
    __tablename__ = 'class'
    id = Column(Integer(unsigned=True),nullable=False,autoincrement=True,primary_key=True)
    major_id = Column(Integer(unsigned=True),ForeignKey('major.id'),nullable=False)
    name = Column(VARCHAR(255),nullable=False)

class Building(BaseModelORM):
    __tablename__ = 'building'
    id = Column(Integer(display_width=10, unsigned=True), nullable=False, autoincrement=True, primary_key=True)
    communityid = Column(Integer(unsigned=True),ForeignKey('community.id'),nullable=False)

    deliver_id = Column(ForeignKey('shop_manager.manager_id'),nullable=True)
    #one building has one shop_manager
    # shopManager = relationship('ShopManager', backref("Building"),uselist=False)

    #one building has many students
    # students = relationship('Student',backref('Building'))

    name = Column(VARCHAR(10), nullable=False, default='1')
    nickname = Column(VARCHAR(255))
    distributionid = Column(Integer(display_width=11))
    maxlat = Column(Float)
    maxlng = Column(Float)
    minlat = Column(Float)
    minlng = Column(Float)
    status = Column(tinyInt(display_width=4), nullable=False, default=0)
    attr = Column(VARCHAR(255))
    addtime = Column(Integer(display_width=11))
    modtime = Column(Integer(display_width=11))
    # indexs = Column(tinyInt(display_width=4), nullable=False, default=0)



class ShopManager(BaseModelORM):
    __tablename__ = 'shop_manager'
    manager_id = Column(Integer(display_width=10, unsigned=True), nullable=False, autoincrement=True,primary_key=True)
    building_id = Column(Integer(display_width=10, unsigned=True), ForeignKey('building.id'))
    manager_name = Column(VARCHAR(255))
    username = Column(CHAR(11), nullable=False)
    password = Column(VARCHAR(32), nullable=False)
    school_id = Column(Integer(display_width=11))
    qq = Column(VARCHAR(255))
    sex = Column(tinyInt(display_width=1)) #男0女1
    graduate_year = Column(CHAR(4))
    alipay_account = Column(VARCHAR(255))
    certificate = Column(VARCHAR(255))
    ID_card = Column(VARCHAR(255))
    dorm = Column(VARCHAR(255))
    email = Column(VARCHAR(255))
    state = Column(tinyInt(display_width=1), default=0)
    ID_number = Column(VARCHAR(255))


class Shop(BaseModelORM):
    __tablename__ = 'shop'
    id = Column(Integer(unsigned=True),nullable=False,primary_key=True)
    community_id = Column(Integer(unsigned=True,display_width=10),ForeignKey("community.id"),nullable=True)
    username = Column(CHAR(11),nullable=False)
    password = Column(CHAR(32), nullable=False)
    name = Column(VARCHAR(50), nullable=False)
    location = Column(VARCHAR(100), nullable=False)
    tel = Column(VARCHAR(20), nullable=False)
    ID_card = Column(TEXT,nullable=False)
    legalman_name = Column(VARCHAR(20),nullable=False)
    alipay_account = Column(VARCHAR(50))
    business_license = Column(TEXT,nullable=False)
    register_ID = Column(VARCHAR(255))
    black1_price = Column(Integer(unsigned=True,display_width=10),nullable=False)
    black2_price = Column(Integer(unsigned=True,display_width=10),nullable=False)
    colorful1_price = Column(Integer(unsigned=True,display_width=10),nullable=False)
    colorful2_price = Column(Integer(unsigned=True,display_width=10),nullable=False)
    state = Column(tinyInt(display_width=1), default=1, nullable=False)
    #{0:已开店，1:未审核，2:已审核，3:已关闭}


class Student(BaseModelORM):
    __tablename__ = 'student'
    student_id = Column(Integer(display_width=10, unsigned=True),nullable=False, autoincrement=True, primary_key=True)
    building_id = Column(Integer(display_width=10, unsigned=True), ForeignKey('building.id'))
    community_id = Column(Integer(unsigned=True),ForeignKey('community.id'))
    academy_id = Column(Integer(unsigned=True),ForeignKey('academy.id'))
    major_id = Column(Integer(unsigned=True),ForeignKey('major.id'))
    class_id = Column(Integer(unsigned=True),ForeignKey('class.id'))
    openid = Column(VARCHAR(255))
    # one student has many print orders
    # printOrders = relationship('PrintOrder',backref('Student'))

    nickname = Column(VARCHAR(255), nullable=False)
    username = Column(CHAR(11), nullable=False)
    password = Column(CHAR(32), nullable=False)
    enroll_year = Column(CHAR(4), nullable=False)



class PrintOrder(BaseModelORM):
    __tablename__ = 'print_order'
    order_id = Column(Integer(display_width=10, unsigned=True), nullable=False, autoincrement=True, primary_key=True)
    trade_no = Column(VARCHAR(255), nullable=False)
    total_pages = Column(Integer(display_width=11), nullable=False)

    student_id = Column(Integer(10, unsigned=True), ForeignKey('student.student_id'),nullable=False)
    building_id = Column(Integer(display_width=10, unsigned=True), ForeignKey('building.id'), nullable=False)
    shop_id = Column(Integer(display_width=10, unsigned=True), ForeignKey('shop.id'), nullable=False)
    manager_id = Column(Integer(display_width=10, unsigned=True), ForeignKey('shop_manager.manager_id'), nullable=False)
    ads_id = Column(ForeignKey('ads.ads_id'))

    charge_id = Column(VARCHAR(255), nullable=True)
    file_url = Column(VARCHAR(100), nullable=True)
    #one print order has many print tasks
    # printTasks = relationship('PrintTask',backref('PrintOrder'))

    #one print order has one advertisement
    # advertise = relationship('Advertise',backref('PrintOrder'),uselist = False)

    print_fee = Column(Integer(display_width=10, unsigned=True), nullable=False)
    delivery_fee = Column(Integer(display_width=10, unsigned=True), nullable=False, default=0)
    student_room = Column(VARCHAR(255), nullable=False)
    student_phone = Column(CHAR(11), nullable=False)
    note = Column(VARCHAR(255), nullable=False)
    note_at = Column(TimeStamp, nullable=True)
    demand_time = Column(VARCHAR(255), nullable=True)
    created_at = Column(TimeStamp, nullable=True, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    paid_at = Column(TimeStamp, nullable=True)
    printed_at = Column(TimeStamp, nullable=True)
    canceled_at = Column(TimeStamp, nullable=True)
    delivered_at = Column(TimeStamp, nullable=True)
    comment_at = Column(TimeStamp, nullable=True)
    comment = Column(VARCHAR(255), nullable=True)
    complain = Column(VARCHAR(255), nullable=True)
    complain_at = Column(TimeStamp, nullable=True)
    rate = Column(tinyInt(display_width=1), default=0, nullable=True)
    emergency = Column(tinyInt(display_width=1), default=0, nullable=True)
    order_state = Column(tinyInt(display_width=1), default=0, nullable=False)
    order_type = Column(tinyInt(display_width=1), default=0, nullable=False) #0 是打印，1是复印



class PrintTask(BaseModelORM):
    __tablename__ = 'print_task'
    task_id = Column(Integer(display_width=10, unsigned=True), nullable=False, autoincrement=True, primary_key=True)

    order_id = Column(ForeignKey('print_order.order_id'))
    student_id = Column(Integer(display_width=10, unsigned=True), ForeignKey('student.student_id'),nullable=False)

    file_name = Column(VARCHAR(255), nullable=False)
    pages = Column(Integer(display_width=10, unsigned=True), nullable=True)
    file_url = Column(VARCHAR(100),nullable=True)
    file_key = Column(CHAR(32), nullable=True)
    bothside = Column(tinyInt(display_width=1), default=0, nullable=False)
    colorful = Column(tinyInt(display_width=1), default=0, nullable=False)
    copies = Column(Integer(display_width=10, unsigned=True), default=1, nullable=False)
    handouts = Column(tinyInt(1), nullable=False, default=1)
    print_fee = Column(Integer(display_width=10,unsigned=True))
    print_pages = Column(Integer(display_width=10,unsigned=True))
    uploaded_at = Column(TimeStamp, nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    updated_at = Column(TimeStamp, nullable=True)
    deleted_at = Column(TimeStamp, nullable=True)
    downloaded_at = Column(TimeStamp, nullable=True)
    task_state = Column(tinyInt(display_width=1), nullable=False, default=0)


class Company(BaseModelORM):
    __tablename__ = 'company'
    company_id = Column(Integer(display_width=10, unsigned=True), nullable=False, autoincrement=True,primary_key=True)
    account = Column(VARCHAR(255), nullable=False)
    password = Column(CHAR(32), nullable=False)
    manager_name = Column(VARCHAR(255), nullable=False)
    manager_phone = Column(VARCHAR(11))
    ID_no = Column(VARCHAR(255))
    ID_pic = Column(TEXT)
    company_name = Column(VARCHAR(255), nullable=False)
    company_type = Column(VARCHAR(255))
    company_address = Column(VARCHAR(255))
    legalman_name = Column(VARCHAR(255))
    business_license = Column(TEXT)
    contract = Column(VARCHAR(255))
    register_ID = Column(VARCHAR(255))
    state = Column(tinyInt(display_width=1), default=0, nullable=False)
    balance = Column(Integer(display_width=11), nullable=False, default=10000)




class Advertise(BaseModelORM):
    __tablename__ = 'ads'
    ads_id = Column(Integer(display_width=10, unsigned=True), nullable=False, autoincrement=True, primary_key=True)
    ads_content = Column(VARCHAR(255), nullable=False)
    ads_url = Column(VARCHAR(255), nullable=False)
    create_at = Column(TimeStamp, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    company_id = Column(Integer(display_width=10, unsigned=True), ForeignKey('company.company_id'),nullable=False)
    city_ids = Column(VARCHAR(255))
    school_ids = Column(VARCHAR(255))
    stu_set = Column(tinyInt(1))
    stu_major = Column(VARCHAR(255))
    stu_enroll_year = Column(VARCHAR(255))
    start = Column(TimeStamp)
    end = Column(TimeStamp)


if __name__ == '__main__':
     engine = create_engine("mysql+pymysql://root:@localhost/CloudPrinter?charset=utf8", echo=True, encoding='utf8')
     Base.metadata.create_all(engine)
     import time
     adv = Advertise(ads_id=10)














