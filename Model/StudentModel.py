# -*- coding: utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.sql.expression import and_, or_
from Model.BaseModel import BaseModel
from sqlalchemy import and_
from Model.ORM import *
import tornado.web
import hashlib
import time

class StudentModel(BaseModel):

    def signup(self,**kwargs):
        try:
            session = self.session
            redisconn = self.redisconn
            if not redisconn.exists(kwargs['username']):
                return self.err("验证码已过期")

            if redisconn.get(kwargs['username']) != kwargs['smsCode'].encode('utf-8'):
                return self.err("验证码错误")
            kwargs['password']=hashlib.md5((kwargs['password']+':'+kwargs['username']).encode()).hexdigest()
            del kwargs['smsCode']
            student = Student(**kwargs)
            session.add(student)
            session.commit()
            token = hashlib.md5((student.username + ':' +str(time.time()) + student.password).encode()).hexdigest()
            week = 60 * 60 * 24 * 7
            redisconn.setex(token,student.student_id,week)
            return self.success([{'token':token}],"注册成功")
        except Exception as emsg:
            return self.err(emsg)

    def resetPassword(self,**kwargs):
        try:
            session = self.session
            redisconn = self.redisconn
            if not redisconn.exists(kwargs['username']):
                return self.err("验证码已过期")

            if redisconn.get(kwargs['username']) != kwargs['smsCode'].encode('utf-8'):
                return self.err("验证码错误")
            kwargs['password']=hashlib.md5((kwargs['password']+':'+kwargs['username']).encode()).hexdigest()
            student = session.query(Student).filter(Student.username==kwargs['username']).first()
            session.query(Student).filter(Student.username==kwargs['username']).update({'password':kwargs['password']})
            session.commit()
            token = hashlib.md5((kwargs['username'] + ':' +str(time.time()) + kwargs['password']).encode()).hexdigest()
            week = 60 * 60 * 24 * 7
            redisconn.setex(token,student.student_id,week)
            return self.success([],"重置密码成功")
        except Exception as emsg:
            return self.err(emsg)


    def login(self,username,password):
        try:
            session = self.session
            redisconn = self.redisconn
            student = session.query(Student).filter(Student.username == username).first()
            if not student:
                return self.err("该账号尚未注册")
            if  student and student.password == hashlib.md5((password+':'+username).encode()).hexdigest():
                token = hashlib.md5((student.username + ':' +str(time.time()) + student.password).encode()).hexdigest()
                week = 60 * 60 * 24 * 7
                redisconn.setex(token, student.student_id,week)
                results = self.studentInfo(student.student_id)
                results['token']=token
                return self.success([results],"登陆成功")
            else:
                return self.err("密码或账号不正确")
        except Exception as emsg:
            return self.err(emsg)

    def firstCheck(self,student_id):
        return self.success([self.studentInfo(student_id)],"token有效")

    #登陆后的
    def studentInfo(self,student_id):
        session = self.session
        baseinfo = session.query(Student.username,Student.building_id,Student.nickname,Student.community_id,Student.openid).filter(Student.student_id==student_id).first()
        results = dict()
        results['studentInfo']=dict(zip(['username','building_id','nickname','community_id','openid'],baseinfo))
        copyTasks = session.query(PrintTask.task_id,PrintTask.file_name,PrintTask.pages,PrintTask.bothside,PrintTask.colorful,PrintTask.copies,PrintTask.handouts)\
            .filter(and_(PrintTask.student_id==student_id,PrintTask.task_state == 0,PrintTask.file_key==None)).all()
        printTasks= session.query(PrintTask.task_id,PrintTask.file_name,PrintTask.pages,PrintTask.bothside,PrintTask.colorful,PrintTask.copies,PrintTask.handouts)\
            .filter(and_(PrintTask.student_id==student_id,PrintTask.task_state == 0,PrintTask.file_key != None)).all()
        copy = list()
        for item in copyTasks:
            copy.append(dict(zip(['task_id','file_name','pages','bothside','colorful','copies','handouts'],item)))

        printTask = list()

        for item in printTasks:
            printTask.append(dict(zip(['task_id','file_name','pages','bothside','colorful','copies','handouts'],item)))

        results['todoTask']={"print": printTask, "copy" : copy}
        priceRule = session.query(Shop.id,Shop.black1_price,Shop.black2_price,Shop.colorful1_price,Shop.colorful2_price).filter(Shop.community_id==baseinfo.community_id).first()

        if priceRule:
            results["priceRule"]= {
                "oneFace":{
                    "black":priceRule.black1_price/100.0,
                    "colorful":priceRule.colorful1_price/100.0
                },
                "twoFace":{
                    "black":priceRule.black2_price/100.0,
                    "colorful":priceRule.colorful2_price/100.0
                }
            }
            results['shop_id']=priceRule.id

        results['demand_time']=["8点-9点","11点-12点","14点-15点","17点-18 点","21点-22点"]
        return results

    def getStudentInfo(self, **kwargs):
        session = self.session
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']
            collection = session.query(Student,Academy, Class, Major, Building, Community).join(Academy, Student.academy_id == Academy.id)\
                                                                             .join(Class, Student.class_id == Class.id)\
                                                                             .join(Major, Student.major_id == Major.id)\
                                                                             .join(Building, Building.id == Student.building_id)\
                                                                             .join(Community, Student.community_id == Community.id)\
                                                                             .filter(Student.student_id == studentid).first()
            results = [dict(
                username = collection[0].username,
                nickname = collection[0].nickname,
                enroll_year = collection[0].enroll_year,
                academyName = collection[1].name,
                className = collection[2].name,
                majorName = collection[3].name,
                buildingName = collection[4].name,
                communityName = collection[5].name,
            )]
            return self.success(results=results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    #Not Finished
    def updateStudentInfo(self, **kwargs):
        session = self.session
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']

            return self.success()
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)


    def updateStuentPass(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']
            oldPassword = kwargs['oldPassword']
            newPassword = kwargs['newPassword']
            collection = session.query(Student).filter(Student.student_id == studentid).first()
            if collection is None:
                return self.err(msg='请注册后登陆')
            if hashlib.md5((collection.username + ':' +str(time.time()) + oldPassword).encode()).hexdigest() != collection.password:
                return self.err('输入原密码错误')
            else:

                student = Student(student_id=studentid,
                                  password=hashlib.md5((collection.username + ':' +str(time.time()) + newPassword).encode()).hexdigest())
                session.merge(student)
                session.commit()
                token = hashlib.md5((collection.username + ':' + str(time.time()) + collection.password).encode()).hexdigest()
                week = 60 * 60 * 24 * 7
                redisconn.setex(token, studentid, week)
            return self.success([],"修改密码成功")
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def bindWechat(self,student_id,openid):
        try:
            session = self.session
            session.query(Student).filter(Student.student_id==student_id).update({'openid':openid})
            session.commit()
            # return self.success([],"绑定微信成功")
            return True
        except Exception,e:
            return False

    def bindWechatStatus(self,student_id):
        try:
            session = self.session
            student = session.query(Student).filter(Student.student_id==student_id).first()
            if student.openid:
                return self.success([],"绑定微信成功")
            else:
                return self.err("暂未成功")
        except Exception,e:
            return self.err(e)

