#encoding=utf-8
__author__ = 'Arthur'
#ID 指的是Access KeyID="vKEirKqFefWcseJ7"，KEY 指的是Access Key Secret="a9Zeg5f39598ASF8cGjNl0oOyWjdjD"
from oss.oss_api import *
import hashlib
import os,sys
import base64
import hmac
import sha
import urllib
import time
import uuid

def CalcSha1(filepath):
    with open(filepath,'rb') as f:
        sha1obj = hashlib.sha1()
        sha1obj.update(f.read())
        hash = sha1obj.hexdigest()
        print(hash)
        return hash

def CalcMD5(filepath):
    with open(filepath,'rb') as f:
        md5obj = hashlib.md5()
        md5obj.update(f.read())
        hash = md5obj.hexdigest()
        print(hash)
        return hash

endpoint="oss-cn-shanghai.aliyuncs.com"
accessKeyId, accessKeySecret="vKEirKqFefWcseJ7","a9Zeg5f39598ASF8cGjNl0oOyWjdjD"
oss = OssAPI(endpoint, accessKeyId, accessKeySecret)
res = oss.create_bucket("uprint-images","private")
res = oss.put_bucket("uprint-images","public-read")
res = oss.put_object_from_file("student-documents", "object_name1", "test.txt")
res = oss.get_object_to_file("student-documents", "object_name1", "test1.txt")
# print "%s\n%s" % (res.status, res.read())
# "http://oss-example.oss-cn-hangzhou.aliyuncs.com/ossapi.pdf?OSSAccessKeyId=44CF9590006BF252F707&Expires=1141889120&Signature=vjbyPxybdZaNmGa%2ByT272YEAiv4%3D"
# Signature = base64(hmac-sha1(AccessKeySecret,
# VERB + "\n"
# + CONTENT-MD5 + "\n"
# + CONTENT-TYPE + "\n"
# + EXPIRES + "\n"
# + CanonicalizedOSSHeaders
# + CanonicalizedResource))

now = int(time.time())+60
objectName = str(uuid.uuid1())
h = hmac.new("a9Zeg5f39598ASF8cGjNl0oOyWjdjD",
             "PUT\n\n\n"+str(now)+"\nstudent-documents/object_name1", sha)

signature = urllib.quote_plus(base64.encodestring(h.digest()).strip())

print "http://student-documents.oss-cn-shanghai.aliyuncs.com/object_name1?Expires="+str(now)+"&OSSAccessKeyId="+accessKeyId+"&Signature="+signature
