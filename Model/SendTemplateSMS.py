__author__ = 'raytlty'
#-*- coding: UTF-8 -*-

import random
import redis
from Model.CCPRestSDK import REST


accountSid= '8a48b551506fd26f01507136100b05c4';


accountToken= '482bd8a5646e48e59850ac371d70e33f';


appId='8a48b551506fd26f01507499940b0bc1';

serverIP='sandboxapp.cloopen.com';

serverPort= '8883';

softVersion='2013-12-26';

def sendTemplateSMS(to,tempId = 1,expire=5):
    rest = REST(serverIP,serverPort,softVersion)
    rest.setAccount(accountSid,accountToken)
    rest.setAppId(appId)
    code = ''
    for i in range(6):
        code += str(random.randint(0,9))
    datas = {code, expire}
    result = rest.sendTemplateSMS(to,datas,tempId)
    if result['statusCode'] == "000000":
        r = redis.StrictRedis('cerulean.me',6379,0)
        r.set(to, code, expire*60)
        return 1
    else:
        return 0

if __name__ == '__main__':
    sendTemplateSMS('13672431102')
