__author__ = 'raytlty'
#-*- coding: UTF-8 -*-


import redis
import logging
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from SDK.PingppSDK import PingPlusPlusSDK
from Configuration.Configurer import Configurer


class BaseModel(object):
    #test environment
    # engine = create_engine("mysql+pymysql://root:root@localhost/CloudPrinter?charset=utf8",
    #                        pool_size=150,pool_recycle=3600,echo=True,encoding='utf8')

    #productive environment
    _engine = create_engine(Configurer().mysql_url,
                           pool_size=20,pool_recycle=3600,echo=False,encoding='utf8')
    _redisConn = redis.Redis(Configurer().redis_url,6379)

    _Session = sessionmaker(_engine)


    @property
    def session(self):
        return self.__class__._Session()

    @property
    def redisconn(self):
        return self.__class__._redisConn

    def err(self, msg='error'):
        return dict(
            error = -1,
            msg = msg,
            results = []
        )

    def success(self, results=None, msg='success'):
        return dict(
            error = 0,
            msg = msg,
            results = results if results else []
        )

if __name__ == '__main__':
    base = BaseModel()
    