# -*- coding: utf-8 -*-
__author__ = 'raytlty'



from Model.BaseModel import BaseModel
from sqlalchemy.sql.expression import and_, desc, asc
from sqlalchemy.sql.functions import max as MAX, min as MIN
from Model.ORM import *
import sys


class AddressModel(BaseModel):

    def getSchool(self, **kwargs):
        session = self.session
        try:
            keyword = kwargs['keyword']
            lastid = kwargs.get('lastid',0)

            items = session.query(Community).filter(and_(Community.name.like('%{}%'.format(keyword)),
                                                         Community.id > lastid)).limit(7).all()
            def getBuilding(communityid):
                buildings = []
                items = session.query(Building).filter(Building.communityid == communityid).all()
                for item in items:
                    buildings.append({'ID':item.id, 'name':item.name, 'status':item.status})
                return sorted(buildings, key=lambda x: x['ID'], reverse=False)
            results = []
            for item in items:
                # print(item)
                results.append({'ID':item.id, 'name':item.name, 'buildings':getBuilding(item.id)})
            return self.success(results=results)
        except Exception as emsg:
            logging.info(emsg)
            return self.err(emsg)

    def getShopSchool(self, **kwargs):
        session = self.session
        try:
            keyword = kwargs['keyword']
            lastid = kwargs.get('lastid',0)

            items = session.query(Community).filter(and_(Community.name.like('%{}%'.format(keyword)),
                                                         Community.id > lastid)).limit(20).all()
            results = []
            for item in items:
                # print(item)
                results.append({'ID':item.id, 'name':item.name})
            return self.success(results=results)
        except Exception as emsg:
            logging.info(emsg)
            return self.err(emsg)

    def getAcademy(self,year,community_id):
        try:
            session = self.session
            res = session.query(Academy.id,Academy.name).filter(Academy.year==year,Academy.community_id == community_id).all()

            results = []

            for item in res:
                results.append(dict(zip(['id','name'],item)))

            return self.success(results)
        except Exception as emsg:
            return self.err(emsg)

    def getMajor(self,academy_id):
        try:
            session = self.session
            res = session.query(Major.id, Major.name).filter(Major.academy_id==academy_id).all()
            results = list()
            for item in res:
                results.append(dict(zip(['id','name'],item)))
            return self.success(results)
        except Exception as emsg:
            return self.err(emsg)

    def getClass(self,major_id):
        try:
            session = self.session
            res = session.query(Class.id, Class.name).filter(Class.major_id==major_id).all()
            results = list()
            for item in res:
                results.append(dict(zip(['id','name'],item)))
            return self.success(results)
        except Exception as emsg:
            return self.err(emsg)

    def test(self, communityid):
        try:
            session = self.session
            collection = session.query(Building).filter(Building.communityid == communityid).first()
            print(collection.name, collection.distributionid)
            building = Building(id=3, distributionid=583)
            session.merge(building)
            session.commit()
            print(collection.name, collection.distributionid)
        except Exception as emsg:
            return self.err(emsg)

    def test2(self, communityid):
        try:
            session = self.session
            # item = session.query(Building).order_by(Building.name.desc()).filter(Building.communityid == communityid).all()
            item = session.query(Building).select_from(Building).order_by(Building.name.asc()).filter(Building.communityid == communityid).all()
            return item
        except Exception as emsg:
            return self.err(emsg)
    def test3(self, shopmanager):
        session = self.session
        session.add(shopmanager)
        session.commit()

    def add_indexs_to_mysql(self):
        session = self.session
        max_community_id = session.query(MAX(Building.communityid)).scalar()
        for community_id in xrange(1, max_community_id+1):
            building = session.query(Building).order_by(Building.id.asc()).filter(community_id == Building.communityid).all()
            for idx in xrange(1, len(building)+1):
                entry = building[idx-1]
                session.query(Building).filter(entry.id == Building.id).update(dict(indexs=idx), synchronize_session=False)
                session.flush()
            session.commit()

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf-8')
    addr = AddressModel()
    # data = dict(
    #     keyword = '长沙',
    #     lastid = None,
    # )
    # items = addr.test2(332)
    # for item in items:
    #   print item.name
    addr.add_indexs_to_mysql()
