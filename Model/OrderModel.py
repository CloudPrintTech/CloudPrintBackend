# -*- coding: utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.sql.expression import and_, or_, desc
from sqlalchemy.sql.functions import max
from Model.BaseModel import BaseModel
from Model.ORM import *
import os
import zipfile
import time
import tornado.web
import tornado.gen
import random
from SDK.PingppSDK import PingPlusPlusSDK as Pingpp
from mywechat_sdk.basic import WechatBasic
from SDK.PingppSDK import PingPlusPlusSDK as Pingpp
import qrcode, base64
from cStringIO import StringIO

class OrderState(object):
    created   = 0  #已创建
    purchased = 1  #已支付
    printed   = 2  #已打印/已复印
    deliverd  = 3  #已配送
    canceled  = 4  #已取消

class OrderModel(BaseModel):
    def createOrder(self,taskIds,student_id,note):
        try:
            printOrder = PrintOrder(student_id=student_id,note=note,)

            session = self.session
            # for taskid in taskIds:
        except Exception, e:
            return self.err(e)
        pass

    def getOrder(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            managerid = kwargs['managerid']
            delivered = kwargs.get('delivered', None)
            lastid = kwargs.get('lastid', 0)
            firstid = kwargs.get('firstid', 0)
            def getNickName(username, student_id): #username == cellphone
                item = session.query(Student).filter(and_(Student.username == username,
                                                                    Student.student_id == student_id)).first()
                return item.nickname

            def GetShopInfo(shopid):
                item = session.query(Shop).filter(Shop.id == shopid).first()
                return dict(shop_id=item.id, shop_name=item.name, shop_location=item.location, shop_tel=item.tel)

            def GetBuildingName(student_id):
                item = session.query(Building).join(Student, Student.building_id == Building.id).filter(Student.student_id == student_id).first()
                return item.name

            def innerFunc(managerid, delivered=None, lastid=0, firstid=0):
                res = []
                if delivered  == None:

                    items = session.query(PrintOrder).order_by(PrintOrder.order_id.desc())
                    if lastid != 0:
                        items = items.filter(and_(PrintOrder.manager_id == managerid,
                                                                  or_(PrintOrder.order_state == OrderState.printed),
                                                                  PrintOrder.order_id < lastid)).limit(20).all()
                    else :
                        if firstid == 0:
                            items = items.filter(and_(PrintOrder.manager_id == managerid,
                                                                  or_(PrintOrder.order_state == OrderState.printed))).limit(20).all()
                        else:
                            items = items.filter(and_(PrintOrder.manager_id == managerid,
                                                                  or_(PrintOrder.order_state == OrderState.printed),
                                                                  PrintOrder.order_id > firstid)).all()
                    for item in items:
                        data = {
                            'ID':item.order_id,
                            'order_state':item.order_state,
                            'student_room': GetBuildingName(item.student_id) + item.student_room,
                            'student_phone':item.student_phone,
                            'nickname': getNickName(item.student_phone, item.student_id),
                            'paid_at':int(time.mktime(time.strptime(item.paid_at.strftime('%Y-%m-%d %H:%M:%S'),'%Y-%m-%d %H:%M:%S'))),
                            'demand_time':item.demand_time,
                            'order_type':item.order_type,
                            'shop':GetShopInfo(item.shop_id),
                            'trade_no':item.trade_no
                        }
                        res.append(data)

                else :
                    items = session.query(PrintOrder).order_by(PrintOrder.order_id.desc())
                    if lastid != 0:
                        items = items.filter(and_(PrintOrder.manager_id == managerid,
                                                                  PrintOrder.order_state == 3,
                                                                  PrintOrder.order_id < lastid)).limit(20).all()
                    else :
                        if firstid == 0:
                            items = items.filter(and_(PrintOrder.manager_id == managerid,
                                                                  PrintOrder.order_state == 3)).limit(20).all()
                        else:
                            items = items.filter(and_(PrintOrder.manager_id == managerid,
                                                                  PrintOrder.order_state == 3,
                                                                  PrintOrder.order_id > firstid)).all()
                    for item in items:
                        data = {
                            'ID':item.order_id,
                            'student_room':GetBuildingName(item.student_id) + item.student_room,
                            'student_phone':item.student_phone,
                            'nickname':getNickName(item.student_phone, item.student_id),
                            'paid_at':int(time.mktime(time.strptime(item.paid_at.strftime('%Y-%m-%d %H:%M:%S'),'%Y-%m-%d %H:%M:%S'))),
                            'rate':item.rate,
                            'demand_time':item.demand_time,
                            'print_fee':item.print_fee / 100.0,
                            'delivery_fee':item.delivery_fee / 100.0,
                            'shop':GetShopInfo(item.shop_id),
                            'order_type':item.order_type,
                            'trade_no':item.trade_no
                        }
                        res.append(data)
                return res
            item = session.query(ShopManager).filter(ShopManager.manager_id == managerid).first()
            if item is None:
                return self.err(msg='请重新登录')
            if item.state == -1:
                return self.err(msg='请审核后再获取订单')
            if item.state == 3:
                return self.err(msg='你已辞职无法获取订单')
            results = innerFunc(managerid, delivered, lastid, firstid)
            if not results:
                return self.success(msg='订单为空')
            else:
                return self.success(results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def getDetail(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            managerid = kwargs['managerid']
            token = kwargs['token']
            order_id = kwargs['order_id']
            def getAds(managerid, order_id):
                ads = []
                advertise = session.query(PrintOrder).filter(and_(PrintOrder.manager_id == managerid, PrintOrder.order_id == order_id)).first()
                item = session.query(Advertise).filter(Advertise.ads_id == advertise.ads_id).first()
                if item is None:
                    return ads
                ads.append({'bothside':item.bothside, 'colorful':item.colorful})
                return ads

            def getTask(order_id):
                tasks = []
                items = session.query(PrintTask).filter(order_id == PrintTask.order_id).all()
                if items is None:
                    return tasks
                for item in items:
                    tasks.append({'file_name':item.file_name, 'pages':item.pages, 'bothside':item.bothside,'colorful':item.colorful,
                                  'copies':item.copies, 'task_id':item.task_id})
                return tasks

            def getStudentinfo(student_id):
                item = session.query(Student).filter(Student.student_id == student_id).first()
                return {'nickname':item.nickname, 'username':item.username}

            # ads = getAds(managerid, order_id)
            tasks = getTask(order_id)
            item = session.query(PrintOrder).filter(PrintOrder.order_id == order_id).first()
            results = []
            results.append(
                    {'fee':item.print_fee / 100.0, 'delivery_fee':item.delivery_fee / 100.0,'note':item.note, 'rate':item.rate,
                     'comment':item.comment, 'tasks':tasks, 'trade_no':item.trade_no},
            )
            return self.success(results=results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    # 确认送达
    def comfirmOrder(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            order_id = kwargs['order_id']
            managerid = kwargs['managerid']
            nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
            order_state = 3 #已送达
            order = session.query(PrintOrder).filter(order_id == PrintOrder.order_id).first()
            session.query(PrintOrder).filter(and_(
                order_id == PrintOrder.order_id,
                managerid == PrintOrder.manager_id
            )).update(dict(delivered_at=nowtime, order_state=order_state),synchronize_session=False)
            session.commit()
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)
        else :
            student = session.query(Student).filter(Student.student_id==order.student_id).first()
            if not student.openid:
                return self.success([{
                    "wechat_response": {
                        "errcode": -1,
                        "errmsg": "该用户未绑定微信"
                    }
                }],msg='确认成功')
            access_token = redisconn.get('access_token')
            ttl = redisconn.ttl('access_token')
            if not ttl:
                ttl = 0
            expireAt = int(time.time())+ttl
            wechat = WechatBasic(appid='wxc324197495b35b29',appsecret='e77411c170a2a569196ddbe78030987a',access_token=access_token,access_token_expires_at=expireAt,token="weixin",checkssl=False)
            if not access_token:
                redisconn.setex('access_token',wechat.access_token,7200)

            data = {
                "first": {
                    "value": "已送达",
                    "color": "#173177"
                },
                "OrderSn":{
                    "value": order.trade_no,
                    "color": "#173177"
                 },
                "OrderStatus":{
                    "value": "订单已送达宿舍楼下",
                    "color": "#173177"
                 },
                 "remark":{
                    "value": "如有问题请及时反馈",
                    "color": "#173177"
                 }
            }
            try:
                response = wechat.send_template_message(user_id=student.openid,
                                         template_id="VgZH4JCfQ3IYybI8yOyJ0jtS_ET-toENbfXXF4RzcRg",data=data)
                return self.success([{'wechat_response':response}])
            except Exception,e:
                return self.success([{
                    "wechat_response": {
                        "errcode": -1,
                        "errmsg": str(e)
                    }
                }],msg='确认成功')

    def getPrinted(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            managerid = kwargs['managerid']
            order_id = kwargs['order_id']
            ads_id = kwargs['ads_id']

            def getTask(order_id):
                items = session.query(PrintTask).filter(PrintTask.order_id == order_id).all()
                return { item.file_url:item.file_name for item in items} if items else {}

            def getAds(ads_id):
                item =  session.query(Advertise).filter(Advertise.ads_id == ads_id).first()
                return (item.ads_url, item.ads_id) if item else (None, None)

            aurl, aname = getAds(ads_id)
            tasks = getTask(order_id)
            zipfile_url= os.path.join(os.path.dirname('__file__'), 'files') + os.sep + str(managerid) \
                           + os.sep + 'order_' + time.strftime("%Y%m%d%H%M%S") + '.zip'
            zfile = zipfile.ZipFile(zipfile_url, 'w', zipfile.ZIP_DEFLATED)
            try:
                for turl, tname in tasks.items():
                    zfile.write(turl, tname)
                zfile.write(aurl, aname)
            except Exception as emsg:
                return self.err(msg=emsg)
            else:
                session.query(PrintOrder).filter(order_id == PrintOrder.order_id).update(dict(file_url=zipfile_url),synchronize_session=False)
                session.commit()
                return self.success(results=[{'file_url':zipfile_url}])
            finally:
                zfile.close()
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def getStudentOrder(self, **kwargs):
        session = self.session
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']
            items = session.query(PrintOrder).filter(PrintOrder.student_id == studentid).all()
            completedOrder = []
            unfinishedOrder = []
            if items is None:
                return self.err(msg='获取学生订单失败,请重新刷新')
            for item in items:
                order_state = item.order_state
                if order_state % 4 and order_state !=4:
                    completedOrder.append(dict(
                        orderId = item.order_id,
                        orderPrice = (item.print_fee + item.delivery_fee),
                        orderPiece = item.total_pages,
                        createTime = item.created_at.strftime('%Y-%m-%d %H:%M:%S'),
                        orderRate = item.rate,
                        orderComment = item.comment,
                        trade_no = item.trade_no,
                    ))
                else:
                    if order_state != 4:
                        unfinishedOrder.append(dict(
                            orderId = item.order_id,
                            orderPrice = (item.print_fee + item.delivery_fee),
                            orderPiece = item.total_pages,
                            createTime = item.created_at.strftime('%Y-%m-%d %H:%M:%S'),
                            trade_no = item.trade_no,
                        ))
            results = [dict(
                token = token,
                order = dict(
                    completedOrder = sorted(completedOrder, key=lambda x: x['createTime'], reverse=False),
                    unfinishedOrder = sorted(unfinishedOrder, key=lambda x: x['createTime'], reverse=False),
                )
            )]
            return self.success(results=results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def cancelStudentOrder(self, **kwargs):
        session = self.session
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']
            order_id = kwargs['order_id']
            canceled_at = time.strftime("%Y-%m-%d %H:%M:%S")
            items = session.query(PrintOrder).filter(and_(PrintOrder.order_id == order_id,
                                                  PrintOrder.student_id == studentid)).first()
            if items.order_state == 2:
                return self.err("此订单已打印，无法取消")
            if items.order_state == 3:
                return self.err("此订单已送达，无法取消")
            if items.order_state == 4:
                return self.err("此订单已取消成功")
            if items.order_state == 1:
                charge_id = items.charge_id
                amount = items.print_fee
                flag = Pingpp.Refund(charge_id, amount, description=u'正在退款')
                if flag is True:
                    item = session.query(PrintOrder).filter(PrintOrder.order_id == order_id).first()
                if item.order_state == 4:
                    return self.success(results=[dict(
                        token = token,
                        orderId = order_id,
                    )],msg=u"取消成功")
                else:
                    return self.err("退款失败")
            if items.order_state == 0:
                try:
                    session.query(PrintOrder).filter(and_(PrintOrder.order_id == order_id,PrintOrder.student_id == studentid)).\
                        update(dict(canceled_at=canceled_at,order_state=4))
                    session.commit()
                    return self.success([],"取消成功")
                except Exception,e:
                    return self.err(str(e))
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(str(emsg))

    def rateStudentOrder(self, **kwargs):
        session = self.session
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']
            order_id = kwargs['order_id']
            rate = kwargs['rate']
            session.query(PrintOrder).filter(and_(PrintOrder.student_id == studentid,
                                                  PrintOrder.order_id == order_id)).update(dict(rate=rate))
            session.commit()
            return self.success(results=[dict(
                token = token,
                orderId = order_id,
            )], msg="评分成功")
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg="评分失败")

    def noteStudentOrder(self, **kwargs):
        session = self.session
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']
            order_id = kwargs['order_id']
            note = kwargs['note']
            notetime = time.strftime("%Y-%m-%d %H:%M:%S")
            session.query(PrintOrder).filter(and_(PrintOrder.student_id == studentid,
                                                  PrintOrder.order_id == order_id)).update(dict(note=note, note_at=notetime),synchronize_session=False)
            session.commit()
            return self.success(results=[dict(
                token = token,
                orderId = order_id
            )])
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def complaintStudent(self, **kwargs):
        session = self.session
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']
            complain = kwargs['complain']
            order_id = kwargs['order_id']
            complaintime = time.strftime("%Y-%m-%d %H:%M:%S")
            session.query(PrintOrder).filter(and_(PrintOrder.student_id == studentid,
                                                  PrintOrder.order_id == order_id)).update(dict(complain=complain, complain_at=complaintime),synchronize_session=False)
            session.commit()
            return self.success()
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def commentStudentOrder(self, **kwargs):
        session = self.session
        try:
            token = kwargs['token']
            studentid = kwargs['studentid']
            order_id = kwargs['order_id']
            rate = kwargs['rate']
            comment = kwargs['comment']
            nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
            session.query(PrintOrder).filter(and_(PrintOrder.student_id == studentid,
                                                  PrintOrder.order_id == order_id)).update(dict(rate=rate, comment=comment,comment_at=nowtime),synchronize_session=False)
            session.commit()
            return self.success()
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)



#############################################
# 与打印店相关

    def GetUnprintOrder(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            shopid = kwargs['shopid']
            lastid = kwargs.get('lastid', 0)
            def GetBuildingName(building_id):
                item = session.query(Building).filter(Building.id == building_id).first()
                return item.name
            def GetStudentNickname(student_id):
                item = session.query(Student).filter(Student.student_id == student_id).first()
                return item.nickname
            def GetLastUploadTime(order_id):
                item = session.query(max(PrintTask.uploaded_at)).filter(PrintTask.order_id == order_id).first()
                return item[0]

            def innerFunc(shopid, lastid=0):
                res = []
                print shopid
                items = session.query(PrintOrder).order_by(PrintOrder.order_id.desc())
                if lastid != 0:
                    items = items.filter(and_(PrintOrder.shop_id == shopid,
                                              PrintOrder.order_type == 0,
                                              PrintOrder.order_state == 1,
                                              PrintOrder.order_id < lastid)).limit(20).all()
                else :
                    items = items.filter(and_(PrintOrder.shop_id == shopid,
                                              PrintOrder.order_type == 0,
                                              PrintOrder.order_state == 1)).limit(20).all()
                print items
                if items is None:
                    return None
                for item in items:
                    uploaded_time = GetLastUploadTime(item.order_id)
                    data = {
                        'order_id':item.order_id,
                        'order_state':item.order_state,
                        'student_room':item.student_room,
                        'student_phone':item.student_phone,
                        'demand_time':item.demand_time,
                        'uploaded_at':uploaded_time.strftime("%Y-%m-%d %H:%M:%S") if uploaded_time else time.strftime("%Y-%m-%d %H:%M:%S"),
                        'student_name':GetStudentNickname(item.student_id),
                        'printed_at':item.printed_at.strftime("%Y-%m-%d %H:%M:%S") if item.printed_at else time.strftime("%Y-%m-%d %H:%M:%S"),
                        'building_name':GetBuildingName(item.building_id),
                        'trade_no':item.trade_no,
                        'student_id':item.student_id,
                    }
                    res.append(data)
                return res
            results = innerFunc(shopid,lastid)
            if not results:
                return self.success(msg='打印订单为空')
            else:
                return self.success(results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def GetUnCopyOrder(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            shopid = kwargs['shopid']
            lastid = kwargs.get('lastid', 0)
            def GetBuildingName(building_id):
                item = session.query(Building).filter(Building.id == building_id).first()
                return item.name
            def GetStudentNickname(student_id):
                item = session.query(Student).filter(Student.student_id == student_id).first()
                return item.nickname
            def GetLastUploadTime(order_id):

                item = session.query(max(PrintTask.uploaded_at)).filter(PrintTask.order_id == order_id).first()
                return item[0]
            def innerFunc(shopid, lastid=0):
                res = []

                items = session.query(PrintOrder).order_by(PrintOrder.order_id.desc())
                if lastid != 0:
                    items = items.filter(and_(PrintOrder.shop_id == shopid,
                                              PrintOrder.order_type == 1,
                                              PrintOrder.order_state == 1,
                                              PrintOrder.order_id < lastid)).limit(20).all()
                else :
                    items = items.filter(and_(PrintOrder.shop_id == shopid,
                                              PrintOrder.order_type == 1,
                                              PrintOrder.order_state == 1)).limit(20).all()
                if items is None:
                    return None
                for item in items:
                    uploaded_time = GetLastUploadTime(item.order_id)
                    data = {
                        'order_id':item.order_id,
                        'order_state':item.order_state,
                        'student_room':item.student_room,
                        'student_phone':item.student_phone,
                        'demand_time':item.demand_time,
                        'uploaded_at': uploaded_time.strftime("%Y-%m-%d %H:%M:%S") if uploaded_time else time.strftime("%Y-%m-%d %H:%M:%S"),
                        'printed_at':item.printed_at.strftime("%Y-%m-%d %H:%M:%S") if item.printed_at else time.strftime("%Y-%m-%d %H:%M:%S"),
                        'building_name':GetBuildingName(item.building_id),
                        'student_name':GetStudentNickname(item.student_id),
                        'trade_no':item.trade_no,
                        'student_id':item.student_id,
                    }
                    res.append(data)
                return res
            results = innerFunc(shopid,lastid)
            if not results:
                return self.success(msg='复印订单为空')
            else:
                return self.success(results=results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    #下载订单文件
    def download_url(self, **kwargs):
        session = self.session
        try:
            def getTask(order_id):
                items = session.query(PrintTask).filter(PrintTask.order_id == order_id).all()
                return { item.file_url.encode('utf-8'):item.file_name.encode('utf-8') for item in items} if items else {}

            def getAds(ads_id):
                item =  session.query(Advertise).filter(Advertise.ads_id == ads_id).first()
                return (item.ads_url, item.ads_id) if item else (None, None)
            student_id = kwargs.get('student_id')
            order_id = kwargs.get('order_id')
            # aurl, aname = getAds(ads_id)
            tasks = getTask(order_id)
            item = session.query(PrintOrder).filter(PrintOrder.order_id == order_id).first()
            zipfile_path= '/root/uprintf/files' + os.sep + 'order_zips' + os.sep + str(student_id)
            if not os.path.exists(zipfile_path):
                os.makedirs(zipfile_path)
            zipfile_name = 'order_' + item.trade_no + '.zip'
            zipfile_url = zipfile_path + os.sep + zipfile_name
            with zipfile.ZipFile(zipfile_url, 'w', zipfile.ZIP_DEFLATED) as zfile:
                for turl, tname in tasks.items():
                    zfile.write(turl, tname)
                    # zfile.write(aurl, aname)
                session.query(PrintOrder).filter(order_id == PrintOrder.order_id).update(dict(file_url=zipfile_url),synchronize_session=False)
                session.commit()
            return zipfile_name, zipfile_url
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            raise tornado.web.HTTPError(500,reason=emsg)

    def ConfirmedShopOrder(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            shop_id = kwargs['shopid']
            order_id = kwargs['order_id']
            nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
            session.query(PrintOrder).filter(and_(PrintOrder.order_id == order_id,
                                                  PrintOrder.shop_id == shop_id)).update(dict(order_state=2, printed_at=nowtime),synchronize_session=False)
            session.commit()
            return self.success(results=[{'printed_at':nowtime}])
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def UnfinishDetails(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            shopid = kwargs['shopid']
            order_id = kwargs['order_id']
            taskinfo = []
            results = []
            items = session.query(PrintTask).filter(PrintTask.order_id == order_id).all()
            for item in items:
                taskinfo.append(dict(
                    file_name = item.file_name,
                    pages = item.pages,
                    bothside = item.bothside,
                    colorful = item.colorful,
                    handouts = item.handouts,
                    copies = item.copies
                ))
            orderitem = session.query(PrintOrder).filter(PrintOrder.order_id == order_id).first()
            results.append(dict(
                taskinfo = taskinfo,
                orderinfo = dict(
                    demand_time = orderitem.demand_time,
                    rate = orderitem.rate,
                    note = orderitem.note,
                    comment = orderitem.comment
                )
            ))
            return self.success(results=results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    # 订单相关
    def submitStudentOrder(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            studentid = kwargs['studentid']
            taskList = kwargs['taskList']
            orderInfo = kwargs['orderInfo']
            shop_id = kwargs['shop_id']
            shopinfo = session.query(Shop).filter(shop_id == Shop.id).first()
            total_pages = 0
            total_fee = 0

            flag = session.query(PrintOrder).filter(and_(
                PrintOrder.order_state == 0,
                PrintOrder.student_id == studentid
            )).first()

            if flag:
                return self.err('请先完成未支付订单')

            studentinfo = session.query(Student).filter(Student.student_id == studentid).first()
            buildinginfo = session.query(Building).filter(Building.id == studentinfo.building_id).first()
            times = time.time()
            nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
            trade_no = str(int(times)) + str(random.randint(1000,10000))[:15]
            task_callback = []
            for task in taskList:
                task_id = task.get('task_id')
                pages = int(task.get('pages', 1))
                copies = task.get('copies', 1)
                is_colorful = task.get('colorful', 0)
                is_bothside = task.get('bothside', 0)
                handouts = task.get('handouts', 1)
                print_fee = 0
                print_pages = 0

                if is_bothside:
                    print_pages += pages/(handouts*2)
                    if pages%(handouts*2)>0:
                        print_pages +=1
                    if is_colorful:
                        print_fee = print_pages * shopinfo.colorful2_price
                        # print_fee += pages/(handouts*2)*shopinfo.colorful2_price
                        # if pages%(handouts*2) > handouts:
                        #     print_fee += shopinfo.colorful2_price
                        #     print_pages += 1
                        # elif pages%(handouts*2)>0:
                        #     print_fee += shopinfo.colorful1_price
                        #     print_pages += 1
                    else:
                        print_fee += print_pages*shopinfo.black2_price
                        # print_fee += pages/(handouts*2)*shopinfo.black2_price
                        # if pages%(handouts*2) > handouts:
                        #     print_fee += shopinfo.black2_price
                        # elif pages%(handouts*2)>0:
                        #     print_fee += shopinfo.black1_price
                else:
                    print_pages += pages/handouts
                    if pages%handouts > 0:
                        print_pages +=1
                    if is_colorful:
                        print_fee += print_pages * shopinfo.colorful1_price
                        # print_fee += pages / handouts * shopinfo.colorful1_price
                        # if pages % handouts > 0:
                        #     print_fee += shopinfo.colorful1_price
                    else:
                        print_fee += print_pages * shopinfo.black1_price
                        # print_fee += pages / handouts * shopinfo.black1_price
                        # if pages % handouts > 0:
                        #     print_fee += shopinfo.black1_price
                total_fee += print_fee * copies
                total_pages += print_pages
                task_callback.append(dict(
                    task_id = task_id,
                    copies = copies,
                    colorful = is_colorful,
                    bothside = is_bothside,
                    handouts = handouts,
                    updated_at = nowtime,
                    print_fee = print_fee,
                    print_pages = print_pages,
                    task_state = 1
                ))

            printorder = PrintOrder(student_id = studentid,
                                    total_pages = total_pages,
                                    building_id = buildinginfo.id ,
                                    shop_id = shop_id,
                                    manager_id = buildinginfo.deliver_id,
                                    print_fee = total_fee,
                                    student_room = kwargs['student_room'],
                                    student_phone = orderInfo['username'],
                                    note = orderInfo['note'],
                                    note_at = nowtime,
                                    created_at = nowtime,
                                    demand_time = orderInfo['demand_time'],
                                    order_type = orderInfo['order_type'],
                                    trade_no = trade_no)
            session.add(printorder)
            session.commit()
            order_id = printorder.order_id
            for task in task_callback:
                task_id = task.get('task_id')
                data = dict(task, **dict(order_id=order_id, task_state=1))
                session.query(PrintTask).filter(PrintTask.task_id == task_id).update(values=data)
                session.flush()
            session.commit()
            return self.success([{'order_id':printorder.order_id, 'total_price':total_fee / 100.0, 'trade_no':printorder.trade_no}])
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception, e:
            return self.err(str(e))


    def payOrder(self,**kwargs):
        try:
            session = self.session
            redisconn = self.redisconn
            order = session.query(PrintOrder).filter(PrintOrder.order_id==kwargs.get('order_id', 0)).first()
            trade_no = order.trade_no
            expire = 24*60
            redisconn.setex(trade_no, False, expire)
            if order is None:
                return self.err('请先确认下单')

            kwargs['extra']['product_id'] = order.trade_no
            charge, sign = Pingpp.Pay(client_ip=kwargs['client_ip'], amount=order.print_fee,
                                         order_no=trade_no, channel=kwargs['channel'], extra=kwargs['extra'])
            if sign == False:
                print charge
                return self.err("支付失败")
            else:
                session.query(PrintOrder).filter(PrintOrder.order_id == kwargs['order_id']).update({'order_state':0,'charge_id':charge['id']})
                session.commit()
                if kwargs['channel'] == 'wx_pub_qr':
                    url = charge['credential']['wx_pub_qr']
                    qr = qrcode.QRCode(
                        version=2,
                        error_correction=qrcode.constants.ERROR_CORRECT_H,
                        box_size=3,
                        border=3,
                    )
                    qr.add_data(url)
                    img_io = StringIO()
                    qr.make_image().save(img_io, 'PNG')
                    img_io.seek(0)
                    return self.success([{'charge':charge, 'qrcode':base64.b64encode(img_io.getvalue())}], "支付成功")
                return self.success([{'charge':charge}],"支付成功")
        except Exception,e:
            return self.err(e)


    def ComfirmPayment(self, trade_no):
        session = self.session
        redisconn = self.redisconn
        try:
            nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
            session.query(PrintOrder).filter(PrintOrder.trade_no == trade_no).update(dict(order_state=1, paid_at=nowtime),synchronize_session=False)
            session.commit()
            expire = 24*60
            redisconn.setex(trade_no, True, expire)
            # msg = status.notifycallback()
            # print msg
        except Exception, e:
            raise Exception(e)

    def PayCallback_Check(self, trade_no):
        redisconn = self.redisconn
        sign = redisconn.get(trade_no)
        return self.success([{'status':sign}])

    def CancelPayment(self, trade_no, charge_id):
        session = self.session
        try:
            nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
            session.query(PrintOrder).filter(and_(PrintOrder.trade_no == trade_no,
                                             PrintOrder.charge_id == charge_id)).update(dict(canceled_at=nowtime,
                                                                                             order_state=4))
            session.commit()
        except Exception,e:
            raise Exception(e)

    def HistoryOrder(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            shopid = kwargs['shopid']
            lastid = kwargs.get('lastid', 0)
            def GetBuildingName(building_id):
                item = session.query(Building).filter(Building.id == building_id).first()
                return item.name
            def GetStudentNickname(student_id):
                item = session.query(Student).filter(Student.student_id == student_id).first()
                return item.nickname
            def GetLastUploadTime(order_id):
                item = session.query(max(PrintTask.uploaded_at)).filter(PrintTask.order_id == order_id).first()
                return item[0]

            def innerFunc(shopid, lastid=0):
                res = []
                print shopid
                query = session.query(PrintOrder).order_by(PrintOrder.order_id.desc())
                items = query.filter().all()
                if lastid != 0:
                    items = items.filter(and_(PrintOrder.shop_id == shopid,
                                         or_(PrintOrder.order_state == 2, PrintOrder.order_state == 3),
                                         PrintOrder.order_id < lastid)).limit(20).all()
                else :
                    items = items.filter(and_(PrintOrder.shop_id == shopid,
                                         or_(PrintOrder.order_state == 2, PrintOrder.order_state == 3),
                                         PrintOrder.order_state == 1)).limit(20).all()
                print items
                if items is None:
                    return None
                for item in items:
                    uploaded_time = GetLastUploadTime(item.order_id)
                    data = {
                        'order_id':item.order_id,
                        'order_state':item.order_state,
                        'student_room':item.student_room,
                        'student_phone':item.student_phone,
                        'demand_time':item.demand_time,
                        'uploaded_at':uploaded_time.strftime("%Y-%m-%d %H:%M:%S") if uploaded_time else time.strftime("%Y-%m-%d %H:%M:%S"),
                        'student_name':GetStudentNickname(item.student_id),
                        'printed_at':item.printed_at.strftime("%Y-%m-%d %H:%M:%S") if item.printed_at else time.strftime("%Y-%m-%d %H:%M:%S"),
                        'building_name':GetBuildingName(item.building_id),
                        'trade_no':item.trade_no,
                        'student_id':item.student_id,
                    }
                    res.append(data)
                return res
            results = innerFunc(shopid,lastid)
            if not results:
                return self.success(msg='历史订单为空')
            else:
                return self.success(results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)