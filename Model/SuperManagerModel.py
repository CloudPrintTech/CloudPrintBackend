# -*- coding: utf-8 -*-
__author__ = 'raytlty'

from Model.BaseModel import BaseModel
from sqlalchemy.sql.expression import and_
import hashlib
import time
from Model.ORM import *
import tornado.web

class SuperManagerModel(BaseModel):

    def login(self, **kwargs):
        redisconn = self.redisconn
        try:
            username = kwargs['username']
            password = kwargs['password']
            if username == "uprint" and hashlib.md5(password.encode('utf-8')).hexdigest() == hashlib.md5("uprint".encode('utf-8')).hexdigest() :
                    token = hashlib.md5((username + ':' +str(time.time()) + password).encode()).hexdigest()
                    week = 60 * 60 * 24 * 7
                    redisconn.setex(token, True, week)
                    results = list()
                    results.append({'token':token})
                    return self.success(results=results, msg='登陆成功')
            else :
                    return self.err(msg='密码错误')
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)


    def getUncheckedComany(self, **kwargs):
        session = self.session
        try:
            lastid = kwargs.get('lastid', 0)
            items = session.query(Company)
            if lastid != 0:
                items = items.filter(and_(Company.state < 2,
                                          Company.company_id < lastid)).limit(20).all()
            else:
                items = items.filter(Company.state == 0).limit(20).all()
            results = []
            if items:
                for item in items:
                    data = dict(
                        company_id = item.company_id,
                        ID_no = item.ID_no,
                        account = item.account,
                        balance = item.balance,
                        business_license = item.business_license,
                        company_address = item.company_address,
                        company_name = item.company_name,
                        company_type = item.company_type,
                        ID_pic = item.ID_pic,
                        contract = item.contract,
                        legalman_name = item.legalman_name,
                        manager_name = item.manager_name,
                        manager_phone = item.manager_phone,
                        state = item.state,
                        register_ID = item.register_ID,
                    )
                    results.append(data)
            return self.success(results,"返回未审核公司成功") #return a list
        except Exception as emsg:
            return self.err(msg=emsg)


    def getUncheckedShopManager(self, **kwargs):
        session = self.session
        try:
            lastid = kwargs.get('lastid', 0)

            items = session.query(ShopManager).order_by(ShopManager.manager_id)
            if lastid != 0:
                items = items.filter(and_(ShopManager.state < 2,
                                          ShopManager.manager_id < lastid)).limit(20).all()
            else :
                items = items.filter(ShopManager.state == 0).limit(20).all()
            results = []
            if items:
                for item in items :
                    data = dict(
                        manager_name = item.manager_name,
                        manager_id = item.manager_id,
                        alipay_account = item.alipay_account,
                        building_id = item.building_id,
                        certificate = item.certificate,
                        dorm = item.dorm,
                        email = item.email,
                        graduate_year = item.graduate_year,
                        ID_number = item.ID_number,
                        ID_card = item.ID_card,
                        qq = item.qq,
                        sex = item.sex,
                        username = item.username,
                        school_id = item.school_id,
                        state = item.state
                    )
                    results.append(data)
            return self.success(results,"返回未审核楼长成功")
        except Exception as emsg:
            return self.err(msg=emsg)

    def getUncheckedShop(self, **kwargs):
        session = self.session
        try:
            lastid = kwargs.get('lastid', 0)

            items = session.query(Shop).order_by(Shop.id)
            if lastid != 0:
                items = items.filter(and_(Shop.state < 2,
                                          Shop.id < lastid)).limit(20).all()
            else :
                items = items.filter(Shop.state == 0).limit(20).all()
            results = []
            if items :
                for item in items :
                    data = dict(
                        community_id = item.community_id,
                        shop_id = item.id,
                        alipay_account = item.alipay_account,
                        name = item.name,
                        legalman_name = item.legalman_name,
                        business_license = item.business_license,
                        register_ID = item.register_ID,
                        oneside_price = item.oneside_price,
                        bothside_price = item.bothside_price,
                        ID_card = item.ID_card,
                        colorful_price = item.colorful_price,
                        location = item.location,
                        username = item.username,
                        tel = item.tel,
                        state = item.state
                    )
                    results.append(data)
            return self.success(results,"返回未审核打印店成功")
        except Exception as emsg:
            return self.err(msg=emsg)

    def companyChecked(self, **kwargs):
        session = self.session
        try:
            isPassed = kwargs['isPassed']
            company_id = kwargs['company_id']
            if isPassed is 1:
                data = {'state' : 0}
            else:
                data = {'state' : -1}
            session.query(Company).filter(Company.company_id == company_id).update(values=data,synchronize_session=False)
            session.commit()
            return self.success([],"审核公司成功") if isPassed == 1 else self.err('审核公司不成功')
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)



    def shopManagerChecked(self, **kwargs):
        session = self.session
        try:
            isPassed = kwargs['isPassed']
            manager_id = kwargs['manager_id']
            if isPassed is 1:
                data = {'state' : 0}
            else:
                data = {'state' : -1}
            session.query(ShopManager).filter(ShopManager.manager_id == manager_id).update(values=data,synchronize_session=False)
            session.commit()
            return  self.success([],"审核楼长成功") if isPassed == 1 else self.err('审核楼长不成功')
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)

    def ShopChecked(self, **kwargs):
        session = self.session
        try:
            isPassed = kwargs['isPassed']
            shop_id = kwargs['shop_id']
            if isPassed is 1:
                data = {'state' : 0}
            else:
                data = {'state' : -1}
            session.query(Shop).filter(Shop.id == shop_id).update(values=data,synchronize_session=False)
            session.commit()
            return  self.success([],"审核打印店成功") if isPassed == 1 else self.err('审核打印店不成功')
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)



