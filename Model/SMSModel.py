# -*- coding: utf-8 -*-
__author__ = 'raytlty'

import random
from Model.CCPRestSDK import REST
from Model.BaseModel import BaseModel
import re

class SMSModel(BaseModel):
    accountSid= '8a48b551506fd26f01507136100b05c4';
    accountToken= '482bd8a5646e48e59850ac371d70e33f';
    appId='8a48b551506fd26f01507499940b0bc1';
    # serverIP='sandboxapp.cloopen.com';
    serverIP='app.cloopen.com';
    serverPort= '8883';
    softVersion='2013-12-26';

    def sendTemplateSMS(self,to,tempId = 50540,expire=5):
        # phonePattern  = re.compile(r'(1)(3\d|4[5,7]|5[0-3,5-9]|8[0,2,3,6-9])\D*(\d{4})\D*(\d{4})$')
        phonePattern = re.compile(r'1[3,4,5,7,8]\d{9}')
        match = phonePattern.match(to)
        if not match:
            return self.err("手机号格式错误")
        redisconn = self.redisconn
        if redisconn.exists('smsCode'+to):
            return self.err("您需要等待"+str(redisconn.ttl('smsCode'+to))+"s才能再次发送验证码。")
        redisconn.set('smsCode'+to,'expireKey',60)
        # 短信验证REST SDK
        rest = REST(SMSModel.serverIP,SMSModel.serverPort,SMSModel.softVersion)
        rest.setAccount(SMSModel.accountSid,SMSModel.accountToken)
        rest.setAppId(SMSModel.appId)
        code = ''
        for i in range(6):
            code += str(random.randint(0,9))
        datas = [code, expire]
        result = rest.sendTemplateSMS(to,datas,tempId)
        if result['statusCode'] == "000000":
            redisconn.set(to, code, expire*60)
            return self.success([],"短信验证码发送成功")
        else:
            return self.err("短信验证码发送失败")

if __name__ == '__main__':
    model = SMSModel()
    model.sendTemplateSMS('13672431102')