__author__ = 'raytlty'


from Model.BaseModel import BaseModel
from Model.ORM import *
import tornado.web

class AdvertiseModel(BaseModel):
    def getAds(self, **kwargs):
        session = self.session
        redisconn = self.redisconn
        try:
            token = kwargs['token']
            managerid = kwargs['managerid']
            lastid = kwargs.get('lastid', 0)
            items = session.query(PrintOrder.ads_id).filter(PrintOrder.manager_id == managerid).all()
            results = []
            for item in items:
                advertise= session.query(Advertise).filter(Advertise.ads_id == item.ads_id)
                results.append({'ads_id':item.ads_id, 'ads_content':advertise.ads_content, 'ads_url': advertise.ads_url})
            return self.success(results=results)
        except KeyError:
            raise tornado.web.HTTPError(400)
        except Exception as emsg:
            return self.err(msg=emsg)