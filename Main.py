#-*- coding: UTF-8 -*-
__author__ = 'raytlty'

import sys
import socket
import tornado.ioloop
import tornado.httpserver
import tornado.web
import tornado.escape
import tornado.gen
import tornado.options
from tornado.netutil import bind_sockets
from tornado.options import define, options
from ManagerController.AddressController import *
from ManagerController.ShopController import *
from ManagerController.SMSController import *
from ManagerController.OrderController import *
from ManagerController.AdvertiseController import *
from SuperManager.LoginHandler import *
from SuperManager.CompanyHandler import *
from SuperManager.ShopManagerHandler import *
from SuperManager.ShopPrinterHandler import *
from Model.AddressModel import AddressModel
from Model.ShopModel import ShopModel
from Model.SMSModel import SMSModel
from Model.OrderModel import OrderModel
from Model.AdvertiseModel import AdvertiseModel
from Model.SuperManagerModel import SuperManagerModel
import StudentController.Signup
import StudentController.Login
import StudentController.PrintTask
import StudentController.StudentOrder
import Model.CheckAccountModel
from AddressController import Address
from Model.StudentModel import StudentModel
from PrinterShopController import PrinterShopController
import StudentController.PrintTask
from Model.PrintTaskModel import PrintTaskModel
from wechat_sdk import WechatBasic
from Configuration.Configurer import Configurer

from SDK.PingppSDK import PingPlusPlusSDK
from PaySystem import PingppWebhook

define('files_path', default=os.path.join(os.path.dirname('__file__'), 'files'))


class ValidWechat(BaseHandler):
    def get(self, *args, **kwargs):
        token = 'weixin'
        wechat = WechatBasic(token=token)
        signature = self.get_argument('signature')  # Request 中 GET 参数 signature
        timestamp = self.get_argument('timestamp')  # Request 中 GET 参数 timestamp
        nonce = self.get_argument('nonce')  # Request 中 GET 参数 nonce
        echostr = self.get_argument('echostr')
        if wechat.check_signature(signature=signature, timestamp=timestamp, nonce=nonce):
            self.finish(echostr)

class Application(tornado.web.Application):
    def __init__(self):

        config = Configurer()

        handlers = [
            (r'/v1/manager/smsCode', ManagersmsCodeHandler),
            (r'/v1/address/school', SelectSchoolHandler),
            (r'/v1/manager/signup', ManagerSignUpHandler),
            (r'/v1/manager/info', ManagerCompleteinfoHandler),
            (r'/v1/manager/password', ChangePasswordHandler),
            (r'/v1/manager/login', ManagerLoginHandler),
            (r'/v1/manager/logout', ManagerLogoutHandler),
            (r'/v1/manager/info/update', UpdateManagerinfoHandler),
            (r'/v1/manager/password/update', UpdateManagerPasswdHandler),
            (r'/v1/manager/orders', ManagerOrderHanlder),
            (r'/v1/manager/resign', ManagerResignHandler),
            (r'/v1/shop/complaints', ManagerComlainHandler),
            (r'/v1/ads', AdsGetHandler),
            (r'/v1/orders/detail', OrdersDetailHandler),
            (r'/v1/orders/confirm', OrdersConfirmHandler),
            (r'/v1/order/printed', OrdersPrintedHandler),

            (r'/v1/check/login',SuperManagerLoginHandler),
            (r'/v1/companies/uncheck', UncheckedCompanies),
            (r'/v1/companies/checked', UncheckedCompanies),
            (r'/v1/shopManager/uncheck', UncheckedShopManager),
            (r'/v1/shopManager/checked', UncheckedShopManager),
            (r'/v1/shop/uncheck',UncheckedShop),
            (r'/v1/shop/checked', UncheckedShop),
            # C端API
            (r'/v1/student/smsCode',StudentController.Signup.SendSMSCode),
            (r'/v1/school/academy',Address.GetAcademy),
            (r'/v1/academy/major',Address.GetMajor),
            (r'/v1/major/class',Address.GetClass),
            (r'/v1/student/signup',StudentController.Signup.Signup),
            (r'/v1/student/login',StudentController.Login.Login),
            #获取二维码
            (r'/v1/student/wechatQrCode',StudentController.Login.GetUrlQrCode),
            #绑定微信账号
            (r'/v1/student/bindWechat',StudentController.Login.BindWechat),
            #确认是否已经绑定成功 : student/bindWechatStatus
            (r'/v1/student/bindWechatStatus',StudentController.Login.BindWechatStatus),

            (r'/v1/student/checkToken',StudentController.Login.checkToken),
            (r'/v1/printTask/beginCreate',StudentController.PrintTask.BeginCreate),
            (r'/v1/printTask/upload',StudentController.PrintTask.UploadTask),

            (r'/v2/printTask/upload',StudentController.PrintTask.UploadTaskV2),

            (r'/v1/printTask/pages',StudentController.PrintTask.GetPages),
            (r'/v1/printTask/create',StudentController.PrintTask.CreateTask),
            (r'/v1/printTask/delete',StudentController.PrintTask.CancleTask),
            (r'/v1/order/create',StudentController.PrintTask.CreateTask),

            (r'/v1/student/info', StudentController.StudentOrder.StudentInfoHandler),
            (r'/v1/student/orders', StudentController.StudentOrder.GetOrderHandler),
            (r'/v1/student/cancel', StudentController.StudentOrder.CancleOrderHandler),
            (r'/v1/student/rate', StudentController.StudentOrder.RateOrderHandler),
            (r'/v1/student/note', StudentController.StudentOrder.NoteOrderHandler),
            (r'/v1/complaint/make', StudentController.StudentOrder.ComplainOrderHandler),
            (r'/v1/order/comment', StudentController.StudentOrder.CommentOrderHandler),
            (r'/v1/order/submit', StudentController.StudentOrder.SubmitOrderHandler),
            #支付订单
            (r'/v1/order/pay', StudentController.StudentOrder.PayOrder),
            (r'/v1/order/wxpaystatus', StudentController.StudentOrder.PayCallback),
            #忘记密码获取验证码
            (r'/v1/forgetPwd/smsCode', StudentController.Login.ForgetPwdSmsCode),
            #忘记密码，更新
            (r'/v1/student/resetPwd', StudentController.Login.ResetPassword),

            #打印店
            (r'/v1/shop/smsCode', PrinterShopController.ShopSMS),
            (r'/v1/shop/signup', PrinterShopController.ShopSignup),
            (r'/v1/shop/address/school', SelectShopNearSchool),
            (r'/v1/shop/info', PrinterShopController.ShopCompleteInfo),
            (r'/v1/shop/login', PrinterShopController.ShopLogin),
            (r'/v1/shop/logout', PrinterShopController.ShopLogout),
            (r'/v1/shop/password ', PrinterShopController.ShopForgetPassword),
            (r'/v1/order/unprinted',PrinterShopController.ShopOrderUnPrinted),
            (r'/v1/order/uncopy', PrinterShopController.ShopOrderUnCopy),
            #下载订单文件
            (r'/v1/order/download', PrinterShopController.DownloadOrder),

            (r'/v1/order/confirmed', PrinterShopController.ShopConfirmOrder),
            (r'/v1/order/printed/details', PrinterShopController.ShopPrintedDetails),
            (r'/v1/order/copy/details',PrinterShopController.ShopCopyDetails),
            (r'/v1/order/history',PrinterShopController.ShopHistoryOrder),
            #Webhooks
            (r'/v1/webhook/charge', PingppWebhook.WebHookChargeHandler),
            (r'/v1/webhook/refund', PingppWebhook.WebHookRefundHandler),

            #Webchat
            (r'/v1/wechat/validate', ValidWechat)
        ]
        settings = dict(
            baseUrl = config.base_url,
            root_path = os.path.abspath(__file__),
            # static_path = os.path.join(os.path.dirname(__file__) + os.sep + 'Web', 'static'),
            static_path = os.path.join(os.path.dirname(__file__), 'static'),
            # template_path = os.path.join(os.path.dirname(__file__) + os.sep + 'Web', 'template') ,#+ os.sep + 'template',
            template_path = os.path.join(os.path.dirname(__file__), 'template') ,#+ os.sep + 'template',
            debug=True,
            gzip = True,
            default_filepath = options.files_path,
            OSSAccessKeyId = "vKEirKqFefWcseJ7",

            wechatAppid = 'wxc324197495b35b29',
            wechatSecrete='e77411c170a2a569196ddbe78030987a',

            # Model
            AddressModel = AddressModel(),
            SMSModel = SMSModel(),
            ShopModel = ShopModel(),
            OrderModel = OrderModel(),
            AdsModel = AdvertiseModel(),
            SuperManagerModel = SuperManagerModel(),
            CheckStudentAccount = Model.CheckAccountModel.CheckStudentAccount(),
            StudentModel = StudentModel(),
            PrintTaskModel = PrintTaskModel(),
        )
        super(Application,self).__init__(handlers,**settings)



# timestamp = "%Y-%m-%d %H:%M:%S"

if __name__ == "__main__":
    application = Application()
    http_server = tornado.httpserver.HTTPServer(application,xheaders=True)
    sock = bind_sockets(Configurer().port, 'localhost', family=socket.AF_INET)
    http_server.add_sockets(sock)
    tornado.ioloop.IOLoop.instance().start()