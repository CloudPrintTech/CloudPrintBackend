# -*- coding: utf-8 -*-

from BaseController import BaseHandler
from Configuration.Configurer import Configurer
import tornado.web
from tornado.escape import json_decode
import qrcode
import httplib
import requests
import time
import urllib
import os

class Login(BaseHandler):
    def post(self, *args, **kwargs):
        StudentModel = self.StudentModel
        param = json_decode(self.request.body.decode('utf-8'))
        self.MsgResponse(**StudentModel.login(param['username'],param['password']))

class checkToken(BaseHandler):
    def get(self, *args, **kwargs):
        StudentModel = self.StudentModel
        token = self.get_query_argument("token")
        student_id = self.isAuthorized(token)
        self.MsgResponse(**StudentModel.firstCheck(student_id))

class ForgetPwdSmsCode(BaseHandler):
    def get(self, *args, **kwargs):
        SMSModel = self.SMSModel
        self.MsgResponse(**SMSModel.sendTemplateSMS(self.get_argument('username')))

class ResetPassword(BaseHandler):
    def post(self, *args, **kwargs):
        StudentModel = self.StudentModel
        param = json_decode(self.request.body.decode('utf-8'))
        self.MsgResponse(**StudentModel.resetPassword(**param))

class GetUrlQrCode(BaseHandler):
    def get(self, *args, **kwargs):
        token = self.get_argument('token')
        student_id = self.isAuthorized(token)
        appid = 'wxc324197495b35b29'
        redirectUrl = Configurer().base_url + "/v1/student/bindWechat"
        # redirectUrl = 'http%3A%2F%2Fapi.in.uprintf.com%2Fv1%2Fstudent%2FbindWechat'
        getCodeUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?" \
            "appid="+appid+ \
            "&redirect_uri="+urllib.quote(redirectUrl,'-') + \
            "&response_type=code" \
            "&scope=snsapi_base" \
            "&state="+token+"#wechat_redirect"
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        qr.add_data(getCodeUrl)
        im = qr.make_image()
        root_path = "/web/uprintf_web"
        file_path = "/qrcode/"+student_id+str(int(time.time()))+".png"
        im.save(root_path+file_path)
        StudentModel = self.StudentModel
        self.MsgResponse(**StudentModel.success([{'qrImgUrl':file_path}],"获取成功"))

class BindWechat(BaseHandler):
    def get(self, *args, **kwargs):
        token = self.get_argument('state')
        student_id = self.isAuthorized(token)
        try:
            code = self.get_argument('code')
            appid = 'wxc324197495b35b29'
            appSecrete = 'e77411c170a2a569196ddbe78030987a'
            url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='+appid+'&secret='+appSecrete+'&code='+code+'&grant_type=authorization_code'
            r = requests.get(url, verify=False)
            data = json_decode(r.content)
            openid = data['openid']
            studentModel = self.StudentModel
            if studentModel.bindWechat(student_id,openid):
                self.redirect("http://mp.weixin.qq.com/s?__biz=MzA5ODYwODU1NQ==&mid=402638512&idx=1&sn=1ae83dbece122bdbc961621b6bab5f5f#rd")
        except Exception,e:
            self.finish(str(e))

class BindWechatStatus(BaseHandler):
    def get(self, *args, **kwargs):
        token = self.get_argument('token')
        student_id = self.isAuthorized(token)
        studentModel = self.StudentModel
        self.MsgResponse(**studentModel.bindWechatStatus(student_id))

if __name__ == '__main__':
    qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=1,
        )
    qr.add_data("hello")
    im = qr.make_image()
    im.save('hello.png')