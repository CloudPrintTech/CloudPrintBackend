# -*- coding: utf-8 -*-

from BaseController import BaseHandler
import tornado.web
from tornado.escape import json_decode
import os
import time
from Utils.Utils import FuckThePy2
import urllib
from pyPdf import PdfFileWriter, PdfFileReader
from oss.oss_api import *
import commands
import logging
import requests
from tornado.concurrent import Future
from Configuration.Configurer import Configurer
from tornado.httpclient import AsyncHTTPClient
from tornado import gen

@gen.coroutine
def fetch_coroutine(url):
    http_client = AsyncHTTPClient()
    response = yield http_client.fetch(url)
    raise gen.Return(response.body)

import hashlib
if __name__ == "__main__":
    print urllib.quote("kin xu")

class BeginCreate(BaseHandler):
    def get(self, *args, **kwargs):
        token = self.get_argument("token")
        student_id = self.isAuthorized(token)
        PrintTaskModel = self.PrintTaskModel
        self.MsgResponse(**PrintTaskModel.beginCreate(self.settings["OSSAccessKeyId"]))

class UploadTask(BaseHandler):
    def get(self, *args, **kwargs):
        self.render("upload.html")
    # @gen.coroutine
    def post(self, *args, **kwargs):
        token = self.get_argument("token")
        student_id = self.isAuthorized(token)
        try:
            PrintTaskModel = self.PrintTaskModel
            files = self.request.files.get('file')
            PrintTaskModel = self.PrintTaskModel
            file_type_list=['pdf','doc','docx','ppt','pptx']
            for f in files:
                file_type = f['filename'].split('.')[-1]
                if file_type not in file_type_list:
                    self.MsgResponse(**PrintTaskModel.err("仅允许上传pdf,ppt,pptx,doc,docx"))
                file_name = "".join(f['filename'].encode("utf-8").split(" "))
                isPdf = False
                if f["content_type"]=="application/pdf":
                    isPdf = True
                dir_path = "/root/uprintf/student-documents/"+ str(student_id) + str(int(time.time()))
                if not os.path.exists(dir_path):
                    os.makedirs(dir_path)
                file_path = dir_path + "/"+ file_name
                md5obj = hashlib.md5()
                md5obj.update(f['body'])
                file_key = md5obj.hexdigest()
                exist = PrintTaskModel.isExisted(file_key)
                file_url = os.path.abspath(file_path)
                # pages = None
                if not exist:
                    with open(file_path, 'wb') as up:
                        up.write(f['body'])     # 写入文件
                        # yield up.write(f['body'])     # 写入文件
                    #上传到oss上保存
                    endpoint= Configurer().oss_end_point
                    # endpoint="oss-cn-shanghai.aliyuncs.com"
                    accessKeyId, accessKeySecret="vKEirKqFefWcseJ7","a9Zeg5f39598ASF8cGjNl0oOyWjdjD"
                    # oss = yield OssAPI(endpoint, accessKeyId, accessKeySecret)
                    # res = yield oss.put_object_from_file("student-documents", str(file_key), file_path)
                    oss = OssAPI(endpoint, accessKeyId, accessKeySecret)
                    res = oss.put_object_from_file("student-documents", str(file_key), file_path)
                if not exist and isPdf:
                    doc = PdfFileReader(file(file_path,"rb"))
                    pages = doc.getNumPages()
                if not exist and (file_type=='ppt' or file_type=='pptx'):
                    os.system("libreoff ice --invisible --convert-to pdf "+ file_path + " --outdir "+dir_path)
                    temp = file_name.split(".")
                    temp[-1]="pdf"
                    pdf_path = dir_path + "/" + ".".join(temp)
                    ppt = PdfFileReader(file(pdf_path,"rb"))
                    pages = ppt.getNumPages()
                if not exist and (file_type=='doc' or file_type=='docx'):
                    try_num = 3
                    while try_num>0:
                        try_num = try_num-1
                        http_client = AsyncHTTPClient()
                        # response = yield http_client.fetch("http://win.in.uprintf.com/v1/file/pages?file_key="+file_key+"&file_type="+file_type)
                        # data = fetch_coroutine("http://win.in.uprintf.com/v1/file/pages?file_key="+file_key+"&file_type="+file_type)
                        data = requests.get("http://win.in.uprintf.com/v1/file/pages?file_key="+file_key+"&file_type="+file_type)
                        # temp = json_decode(data)
                        temp = json_decode(data.content)
                        pages = temp['page']
                        if pages != -1:
                            break
                        # pages == None
                if exist:
                    pages = exist.pages
                    file_url = exist.file_url

                if pages == -1 or pages==None:
                    self.MsgResponse(**PrintTaskModel.err("上传失败"))

                self.MsgResponse(**PrintTaskModel.uploadTask(student_id,file_name,file_key,file_url,pages))
        except Exception,e:
            self.MsgResponse(**PrintTaskModel.err(str(e)))

class UploadTaskV2(BaseHandler):
    def get(self, *args, **kwargs):
        self.render("upload.html")
    # @gen.coroutine
    def post(self, *args, **kwargs):
        token = self.get_argument("token")
        student_id = self.isAuthorized(token)
        try:
            PrintTaskModel = self.PrintTaskModel
            files = self.request.files.get('file')
            PrintTaskModel = self.PrintTaskModel
            file_type_list=['pdf','doc','docx','ppt','pptx']
            for f in files:
                file_type = f['filename'].split('.')[-1]
                if file_type not in file_type_list:
                    self.MsgResponse(**PrintTaskModel.err("仅允许上传pdf,ppt,pptx,doc,docx"))
                file_name = "".join(f['filename'].encode("utf-8").split(" "))
                isPdf = False
                if f["content_type"]=="application/pdf":
                    isPdf = True
                dir_path = "/root/uprintf/student-documents/"+ str(student_id) + str(int(time.time()))
                if not os.path.exists(dir_path):
                    os.makedirs(dir_path)
                file_path = dir_path + "/"+ file_name
                md5obj = hashlib.md5()
                md5obj.update(f['body'])
                file_key = md5obj.hexdigest()
                exist = PrintTaskModel.isExisted(file_key)
                file_url = os.path.abspath(file_path)
                pages = None
                if not exist:
                    with open(file_path, 'wb') as up:
                        up.write(f['body'])     # 写入文件
                        # yield up.write(f['body'])     # 写入文件
                    #上传到oss上保存
                    endpoint= Configurer().oss_end_point
                    # self.MsgResponse(**PrintTaskModel.err(endpoint))
                    # endpoint="oss-cn-shanghai.aliyuncs.com"
                    accessKeyId, accessKeySecret="vKEirKqFefWcseJ7","a9Zeg5f39598ASF8cGjNl0oOyWjdjD"
                    # oss = yield OssAPI(endpoint, accessKeyId, accessKeySecret)
                    # res = yield oss.put_object_from_file("student-documents", str(file_key), file_path)
                    oss = OssAPI(endpoint, accessKeyId, accessKeySecret)
                    res = oss.put_object_from_file("student-documents", str(file_key), file_path)
                if not exist and isPdf:
                    doc = PdfFileReader(file(file_path,"rb"))
                    pages = doc.getNumPages()
                if not exist and (file_type=='ppt' or file_type=='pptx'):
                    os.system("libreoff ice --invisible --convert-to pdf "+ file_path + " --outdir "+dir_path)
                    temp = file_name.split(".")
                    temp[-1]="pdf"
                    pdf_path = dir_path + "/" + ".".join(temp)
                    ppt = PdfFileReader(file(pdf_path,"rb"))
                    pages = ppt.getNumPages()
                if not exist and (file_type=='doc' or file_type=='docx'):
                    try_num = 3
                    while try_num>0:
                        try_num = try_num-1
                        http_client = AsyncHTTPClient()
                        # response = yield http_client.fetch("http://win.in.uprintf.com/v1/file/pages?file_key="+file_key+"&file_type="+file_type)
                        # data = fetch_coroutine("http://win.in.uprintf.com/v1/file/pages?file_key="+file_key+"&file_type="+file_type)
                        data = requests.get("http://win.in.uprintf.com/v1/file/pages?file_key="+file_key+"&file_type="+file_type)
                        # temp = json_decode(data)
                        if data.status_code != 200:
                            break
                        temp = json_decode(data.content)
                        pages = temp['page']
                        if pages != -1:
                            break
                        # pages == None
                    if pages == -1 or data.status_code!=200:
                        os.system("libreoff ice --invisible --convert-to pdf "+ file_path + " --outdir "+dir_path)
                        temp = file_name.split(".")
                        temp[-1]="pdf"
                        pdf_path = dir_path + "/" + ".".join(temp)
                        doc = PdfFileReader(file(pdf_path,"rb"))
                        pages = doc.getNumPages()

                if exist:
                    pages = exist.pages
                    file_url = exist.file_url
                if pages == -1 or pages == None:
                    self.MsgResponse(**PrintTaskModel.err("上传失败"))
                data = []
                task_id = PrintTaskModel.uploadTaskV2(student_id,file_name,file_key,file_url,pages)
                data.append({'pages':pages,'task_id': task_id, 'file_name':f['filename']})
            self.MsgResponse(**PrintTaskModel.success(data))
        except Exception,e:
            self.MsgResponse(**PrintTaskModel.err(str(e)))

class GetPages(BaseHandler):
    def get(self, *args, **kwargs):
        token = self.get_argument("token")
        student_id = self.isAuthorized(token)
        task_id = self.get_query_argument("task_id")
        PrintTaskModel = self.PrintTaskModel
        self.MsgResponse(**PrintTaskModel.getTaskPages(task_id,self.rootPath))

class CreateTask(BaseHandler):
    def post(self, *args, **kwargs):
        param = FuckThePy2(json_decode(self.request.body.decode('utf-8')))
        param['student_id'] = self.isAuthorized(param['token'])
        del param['token']
        PrintTaskModel = self.PrintTaskModel
        self.MsgResponse(**PrintTaskModel.createTask(**param))

class CancleTask(BaseHandler):
    def get(self, *args, **kwargs):
        student_id = self.isAuthorized(self.get_argument('token'))
        PrintTaskModel = self.PrintTaskModel
        self.MsgResponse(**PrintTaskModel.deleteTask(self.get_argument('task_id')))

class Download(BaseHandler):
    def get(self):
        filepath = "./student-documents/31448284127"
        filename= "4086.jpg"
        #Content-Type这里我写的时候是固定的了，也可以根据实际情况传值进来
        self.set_header ('Content-Type', 'application/octet-stream')
        self.set_header ('Content-Disposition', 'attachment; filename='+filename)
        #读取的模式需要根据实际情况进行修改
        buf_size = 64
        with open(filepath+"/"+filename, 'rb') as f:
            while True:
                data = f.read(buf_size)
                if not data:
                    break
                self.write(data)
        #记得有finish哦
        self.finish()

def handle_request(response):
    if response.error:
        print "Error:", response.error
    else:
        print response.body
if __name__ == '__main__':
    # endpoint= Configurer().oss_end_point
    # accessKeyId, accessKeySecret="vKEirKqFefWcseJ7","a9Zeg5f39598ASF8cGjNl0oOyWjdjD"
    # oss = OssAPI(endpoint, accessKeyId, accessKeySecret)
    # res = oss.put_object_from_file("student-documents", 'err.log', os.path.dirname(__file__)+'/Login.py')
    # print res
    # data = fetch_coroutine("http://www.baidu.com")
    data = requests.get('http://www.baidu.com')
    print data.status_code

    # http_client = AsyncHTTPClient()
    # http_client.fetch("http://win.in.uprintf.com/v1/file/pages?file_key=832c80d16574e6b67ff49717b8e1693d&file_type=docx", handle_request)
