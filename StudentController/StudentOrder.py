# -*- coding: utf-8 -*-
__author__ = 'raytlty'

from BaseController import BaseHandler
from tornado.escape import json_decode
from Utils.Utils import FuckThePy2
import tornado.websocket
import tornado.gen
import tornado.escape


# shop/deliverTime
class ChooseDeliverTHandler(BaseHandler):
    def get(self):
        token = self.get_query_argument('token')
        studentid = self.isAuthorized(token)

class CreateOrder(BaseHandler):
    def post(self):
        param = FuckThePy2(self.query.body.decode('utf-8'))
        param['student_id']=self.isAuthorized(param['token'])
        del param['token']


# student/orders
class GetOrderHandler(BaseHandler):
    def get(self):
        OrderModel = self.OrderModel
        token = self.get_query_argument('token')
        studentid = self.isAuthorized(token)
        body_data = dict(
            token = token,
            studentid = int(studentid),
        )
        self.MsgResponse(**OrderModel.getStudentOrder(**body_data))

# student/cancel
class CancleOrderHandler(BaseHandler):
    def post(self):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        studentid = self.isAuthorized(token).decode('utf-8')
        body_data['studentid'] = int(studentid)
        self.MsgResponse(**OrderModel.cancelStudentOrder(**body_data))

# student/rate
class RateOrderHandler(BaseHandler):
    def post(self):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        studentid = self.isAuthorized(token).decode('utf-8')
        body_data['studentid'] = int(studentid)
        self.MsgResponse(**OrderModel.rateStudentOrder(**body_data))

# student/note
class NoteOrderHandler(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        studentid = self.isAuthorized(token).decode('utf-8')
        body_data['studentid'] = int(studentid)
        self.MsgResponse(**OrderModel.noteStudentOrder(**body_data))

# complaint/make
class ComplainOrderHandler(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        studentid = self.isAuthorized(token).decode('utf-8')
        body_data['studentid'] = int(studentid)
        self.MsgResponse(**OrderModel.complaintStudent(**body_data))

# student/info
class StudentInfoHandler(BaseHandler):
    def get(self):
        StudentModel = self.StudentModel
        token = self.get_query_argument('token')
        studentid = self.isAuthorized(token)
        body_data = dict(
            token = token,
            studentid = studentid,
        )
        self.MsgResponse(**StudentModel.getStudentInfo(**body_data))

# order/comment
class CommentOrderHandler(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        studentid = self.isAuthorized(token).decode('utf-8')
        body_data['studentid'] = int(studentid)
        self.MsgResponse(**OrderModel.commentStudentOrder(**body_data))

# order/submit
class SubmitOrderHandler(BaseHandler):

    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        studentid = self.isAuthorized(token).decode('utf-8')
        body_data['studentid'] = int(studentid)
        self.MsgResponse(**OrderModel.submitStudentOrder(**body_data))


class PayOrder(BaseHandler):
    def post(self, *args, **kwargs):
        OrderModel = self.OrderModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        client_ip = self.request.remote_ip
        body_data['client_ip'] = client_ip
        self.isAuthorized(token).decode('utf-8')
        self.MsgResponse(**OrderModel.payOrder(**body_data))

# /order/wxpaystatus
class PayCallback(BaseHandler):
    def get(self, *args, **kwargs):
        OrderModel = self.OrderModel
        token = self.get_argument('token')
        trade_no = self.get_argument('trade_no')
        self.isAuthorized(token)
        self.MsgResponse(**OrderModel.PayCallback_Check(trade_no))
