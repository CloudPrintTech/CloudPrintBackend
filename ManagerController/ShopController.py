# -*- coding: utf-8 -*-
# __author__ = 'raytlty'


from BaseController import BaseHandler
from tornado.escape import json_decode
from tornado.web import MissingArgumentError
import json
import os
from Utils.Utils import FuckThePy2

# manager/signup
class ManagerSignUpHandler(BaseHandler):
    def post(self):
        ShopModel = self.ShopModel
        try:
            username = self.get_body_argument('username')
            password = self.get_body_argument('password')
            smsCode = self.get_body_argument('smsCode')
            body_data  = dict(
                username = username,
                password = password,
                smsCode = smsCode,
            )
        except MissingArgumentError:
            body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        args = body_data
        self.MsgResponse(**ShopModel.signUp(**args))


# manager/info
class ManagerCompleteinfoHandler(BaseHandler):

    def get(self):
        ShopModel = self.ShopModel
        token = self.get_query_argument('token')
        body_data = dict(
            token = token,
        )
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data ['managerid'] = int(managerid)
        self.MsgResponse(**ShopModel.getMyInfo(**body_data))

    def post(self):
        ShopModel = self.ShopModel
        try:
            token = self.get_body_argument('token')
            manager_name = self.get_body_argument('manager_name')
            qq = self.get_body_argument('qq')
            sex = self.get_body_argument('sex')
            email = self.get_body_argument('email')
            graduate_year = self.get_body_argument('graduate_year')
            alipay_account = self.get_body_argument('alipay_account')
            certificate = self.get_body_argument('certificate') #Image
            ID_card = self.get_body_argument('ID_card') #Image
            dorm = self.get_body_argument('dorm')
            ID_number = self.get_body_argument('ID_number')
            school = self.get_body_argument('school')
            building = self.get_body_argument('building')
            body_data = dict(
                token = token,
                manager_name = manager_name,
                qq = qq,
                sex = sex,
                email = email,
                graduate_year = graduate_year,
                alipay_account = alipay_account ,
                certificate = certificate,
                ID_card = ID_card,
                dorm = dorm,
                ID_number = ID_number,
                school = school,
                building = building,
            )
        except MissingArgumentError:
            body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        print('fuck u mother')
        # get user id or raise 401 error
        # 用户验证
        managerid = self.isAuthorized(token).decode('utf-8')
        filePath = os.path.join(self.userFile, str(managerid))
        body_data['managerid'] = int(managerid)
        body_data['filePath'] = filePath
        self.MsgResponse(**ShopModel.completeInfo(**body_data))

# manager/login
class ManagerLoginHandler(BaseHandler):

    def post(self):
        ShopModel = self.ShopModel
        try:
            username = self.get_body_argument('username')
            password = self.get_body_argument('password')
            body_data = dict(
                username = username,
                password = password,
            )
        except MissingArgumentError:
            body_data = json.loads(self.request.body.decode('utf-8'))
        body_data = dict(**FuckThePy2(body_data))
        self.MsgResponse(**ShopModel.login(**body_data))

# manager/logout
class ManagerLogoutHandler(BaseHandler):

    def post(self):
        ShopModel = self.ShopModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**ShopModel.logout(**body_data))


# manager/password
class ChangePasswordHandler(BaseHandler):
    def post(self):
        ShopModel = self.ShopModel
        try:
            username = self.get_body_argument('username')
            password = self.get_body_argument('password')
            smsCode = self.get_body_argument('smsCode')
            body_data = dict(
                username = username,
                password = password,
                smsCode = smsCode,
            )
        except MissingArgumentError:
            body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        self.MsgResponse(**ShopModel.forgetPassword(**body_data))

# manager/info/update
class UpdateManagerinfoHandler(BaseHandler):
    def post(self):
        ShopModel = self.ShopModel
        body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        managerid = self.isAuthorized(token).decode('utf-8')
        filePath = os.path.join(self.userFile, str(managerid))
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**ShopModel.updateInfo(**body_data))

# manager/password/update
class UpdateManagerPasswdHandler(BaseHandler):
    def post(self):
        ShopModel = self.ShopModel
        try:
            token = self.get_body_argument('token')
            oldPassword = self.get_body_argument('oldPassword')
            newPassword = self.get_body_argument('newPassword')
            body_data = dict(
                token = token,
                oldPassword = oldPassword,
                newPassword = newPassword,
            )
        except MissingArgumentError:
            body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**ShopModel.changePassword(**body_data))

# manager/resign
class ManagerResignHandler(BaseHandler):
    def post(self):
        ShopModel = self.ShopModel
        try:
            token = self.get_body_argument('token')
            body_data = dict(
                token = token,
            )
        except MissingArgumentError:
            body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**ShopModel.resign(**body_data))

# shop/complaints
class ManagerComlainHandler(BaseHandler):
    def get(self):
        ShopModel = self.ShopModel
        token = self.get_query_argument('token')
        lastID = self.get_query_argument('lastID',default=0)
        body_data = dict(
            token = token,
            lastid = int(lastID),
        )
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**ShopModel.getComplain(**body_data))

        # d26229da1ee3dd5999a1748fe698fdef