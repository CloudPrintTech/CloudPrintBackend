__author__ = 'raytlty'


from BaseController import BaseHandler
from tornado.web import MissingArgumentError
from tornado.escape import json_decode
from Utils.Utils import FuckThePy2
import json

# manager/orders
class ManagerOrderHanlder(BaseHandler):
    def get(self):
        OrderModel = self.OrderModel
        token = self.get_query_argument('token')
        delivered = self.get_query_argument('type', default=None)
        lastid = self.get_query_argument('lastID', default=0)
        firstid = self.get_query_argument('firstID', default=0)
        body_data = dict(
            token = token,
            delivered = delivered,
            lastid = int(lastid) if lastid else 0,
            firstid = int(firstid) if firstid else 0,
        )
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**OrderModel.getOrder(**body_data))

# orders/detail
class OrdersDetailHandler(BaseHandler):
    def get(self):
        OrderModel = self.OrderModel
        token = self.get_query_argument('token')
        order_id = self.get_query_argument('order_id')
        body_data = dict(
            token = token,
            order_id = order_id,
        )
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**OrderModel.getDetail(**body_data))

# orders/confirm
class OrdersConfirmHandler(BaseHandler):
    def post(self):
        OrderModel = self.OrderModel
        try:
            token = self.get_body_argument('token')
            order_id = self.get_body_argument('order_id')
            body_data = dict(
                token = token,
                order_id = order_id,
            )
        except MissingArgumentError:
            body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**OrderModel.comfirmOrder(**body_data))

# order/printed
class OrdersPrintedHandler(BaseHandler):
    def post(self):
        OrderModel = self.OrderModel
        try:
            token = self.get_body_argument('token')
            order_id = self.get_body_argument('order_id')
            ads_id = self.get_body_argument('ads_id')
            body_data = dict(
                token = token,
                order_id = int(order_id),
                ads_id = int(ads_id),
            )
        except MissingArgumentError:
            body_data = json_decode(self.request.body.decode('utf-8'))
        body_data = FuckThePy2(body_data)
        token = body_data.get('token')
        managerid = self.isAuthorized(token).decode('utf-8')
        body_data['managerid'] = int(managerid)
        self.MsgResponse(**OrderModel.getPrinted(**body_data))


