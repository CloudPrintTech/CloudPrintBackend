# -*- coding: utf-8 -*-
__author__ = 'raytlty'


from BaseController import BaseHandler

# ads 获取今日广告
class AdsGetHandler(BaseHandler):
    def get(self):
        AdsModel = self.AdsModel
        token = self.get_query_argument('token')
        lastid = self.get_query_argument('lastID', default=0)
        managerid = self.isAuthorized(token)
        data = dict(
            token = token,
            managerid = managerid,
            lastid = int(lastid) if lastid else 0,
        )
        self.MsgResponse(**AdsModel.getAds(**data))