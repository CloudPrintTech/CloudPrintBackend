__author__ = 'raytlty'
#-*- coding: UTF-8 -*-

from BaseController import BaseHandler
from tornado.web import MissingArgumentError
from tornado.escape import json_decode
from Utils.Utils import FuckThePy2

# address/school
class SelectSchoolHandler(BaseHandler):
    def get(self):
        AddressModel = self.AddressModel
        keyword = self.get_query_argument('keyword').encode('utf8')
        lastid = self.get_query_argument('lastID',default=0)
        body_data = dict(
            keyword = keyword,
            lastid = int(lastid),
        )
        self.MsgResponse(**AddressModel.getSchool(**body_data))

# shop/address/school
class SelectShopNearSchool(BaseHandler):
    def get(self):
        AddressModel = self.AddressModel
        keyword = self.get_query_argument('keyword').encode('utf8')

        lastid = self.get_query_argument('lastID',default=0)
        body_data = dict(
            keyword = keyword,
            lastid = int(lastid),
        )
        self.MsgResponse(**AddressModel.getShopSchool(**body_data))