__author__ = 'Arthur'
from BaseController import BaseHandler
from tornado.web import MissingArgumentError
from tornado.escape import json_decode

class SearchSchoolAndBuilding(BaseHandler):
    def get(self):
        AddressModel = self.AddressModel
        keyword = self.get_query_argument('keyword')
        lastid = self.get_query_argument('lastID',default=0)
        body_data = dict(
            keyword = keyword,
            lastid = int(lastid)
        )
        self.MsgResponse(**AddressModel.getSchool(**body_data))

class GetAcademy(BaseHandler):
    def get(self, *args, **kwargs):
        AddressModel = self.AddressModel
        self.MsgResponse(**AddressModel.getAcademy(self.get_query_argument('year'),self.get_query_argument('community_id')))

class GetMajor(BaseHandler):
    def get(self, *args, **kwargs):
        AddressModel = self.AddressModel
        self.MsgResponse(**AddressModel.getMajor(self.get_query_argument('academy_id')))

class GetClass(BaseHandler):
    def get(self, *args, **kwargs):
        AddressModel = self.AddressModel
        self.MsgResponse(**AddressModel.getClass(self.get_query_argument('major_id')))