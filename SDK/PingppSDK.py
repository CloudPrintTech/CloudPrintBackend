# -*- coding: utf-8 -*-
__author__ = 'raytlty'

import pingpp
import base64
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
import sys
reload(sys)
sys.setdefaultencoding('utf8')



class PingPlusPlusSDK(object):
    # self.mongodb = pymongo.MongoClient('localhost', 27017).get_database('cloudprint')
    __api_key = 'sk_live_brLK841KOevPKqXHiDW9O08O'
    __app_id = 'app_WX5q5Gv5eD04iDa5'
    __charge = pingpp.Charge
    __redenvelope = pingpp.RedEnvelope
    __test_pkey = 'pk_test_1G4i5C0y9G48044Si5TinznH'

    @classmethod
    def Pay(cls, client_ip, amount, order_no, channel, extra = None, body = None):
        if channel.startswith('alipay'):
            try:
                charge = cls.__charge.create(
                    api_key = cls.__api_key,
                    app = dict(id=cls.__app_id),
                    order_no = order_no,
                    amount = amount,
                    client_ip = client_ip,
                    channel = channel,
                    currency='cny',
                    extra = dict(
                        success_url = extra['success_url'],
                    ),
                    subject = '订单支付',
                    body = body if body else '正在支付，请稍后',
                )
            except pingpp.PingppError as emsg:
                return emsg, False
            else:
                return charge, True
        elif channel == 'wx_pub_qr':
            try:
                charge = cls.__charge.create(
                    api_key = cls.__api_key,
                    app = dict(id=cls.__app_id),
                    order_no = order_no,
                    amount = amount,
                    client_ip = client_ip,
                    channel = channel,
                    currency='cny',
                    extra = dict(
                        product_id = extra['product_id'],
                    ),
                    subject = '订单支付',
                    body = body if body else '正在支付，请稍后',
                )
            except pingpp.PingppError as emsg:
                return emsg, False
            else:
                return charge, True
        elif channel == 'wx_pub':
            try:
                charge = cls.__charge.create(
                    api_key = cls.__api_key,
                    app = dict(id=cls.__app_id),
                    order_no = order_no,
                    amount = amount,
                    client_ip = client_ip,
                    channel = channel,
                    currency='cny',
                    extra = dict(
                        open_id = extra.get('open_id'),
                    ),
                    subject = '订单支付',
                    body = body if body else '正在支付，请稍后',
                )
            except pingpp.PingppError as emsg:
                return emsg, False
            else:
                return charge, True
    @staticmethod
    def VerifySign(signature, webhooks_data):
        try:
            def decode_base64(data):
                missing_padding = 4 - len(data) % 4
                if missing_padding:
                    data += b'=' *missing_padding
                return base64.decodestring(data)

            signature = decode_base64(signature)
            # webhooks_data = open('../SDK/message.json').read()
            pubkey = RSA.importKey(open('/root/uprintf/SDK/my-server.pub').read())
            digest = SHA256.new(webhooks_data)
            pkcs = PKCS1_v1_5.new(pubkey)
            return pkcs.verify(digest, signature)
        except Exception, e:
            raise Exception(e)

    @classmethod
    def Refund(cls, charge_id, amount, description=None):
        try:
            if amount is None:
                return 'amount can\'t be Null', False
            charge = cls.__charge.retrieve(id=charge_id,api_key=cls.__api_key)
            refund = charge.refunds.create(
                description = description if description else 'Refund Description',
                amount = amount
            )
            return True
        except Exception as e:
            return False

    def Redenvelope(self, order_no, open_id, amount, subject=None, body=None):
        pass

def decode_base64(data):
    missing_padding = 4 - len(data) % 4
    if missing_padding:
        data += b'=' *missing_padding
    return base64.decodestring(data)

def verify(sig):
    signs = decode_base64(sig)
    datas = open('../SDK/message.json').read()
    data = datas.encode('utf-8')
    pubkey = RSA.importKey(open('../SDK/my-server.pub').read())
    digest = SHA256.new(data)
    pkcs = PKCS1_v1_5.new(pubkey)
    return pkcs.verify(digest, signs)



if __name__ == '__main__':
    pubkey = RSA.importKey(open('my-server.pub').read())
    print pubkey.keydata
    # print verify(sign)