# -*- coding: utf-8 -*-
__author__ = 'raytlty'

from BaseController import BaseHandler
from tornado.escape import json_decode
from Utils.Utils import FuckThePy2
from SDK.PingppSDK import PingPlusPlusSDK as pingpp
import os

# /webhook/charge
class WebHookChargeHandler(BaseHandler):
    def post(self, *args, **kwargs):
        try:
            data = json_decode(self.request.body.decode('utf-8'))
            data = FuckThePy2(data)
            signature = self.request.headers['X-Pingplusplus-Signature'].decode('utf-8')
            trade_no = data['data']['object']['order_no']
            if data['type'] == 'charge.succeeded':
                self.set_status(status_code=200, reason=u'验证成功')
                reason = u'验证成功'
                self.OrderModel.ComfirmPayment(trade_no)
            else:
                self.set_status(status_code=500, reason=u'服务器错误/验证失败')
                reason = u'服务器错误/验证失败'
        except Exception,e:
            self.set_status(status_code=500, reason=u'服务器错误/验证失败')
            reason = e
        self.finish(reason)

class WebHookRefundHandler(BaseHandler):
    def post(self, *args, **kwargs):
        try:
            data = json_decode(self.request.body.decode('utf-8'))
            data = FuckThePy2(data)

            trade_no = data['data']['object']['order_no']
            charge_id = data['data']['object']['charge']
            reason = ''
            if data['type'] == 'refund.succeeded':
                self.set_status(status_code=200, reason=u'验证成功')
                self.OrderModel.CancelPayment(trade_no, charge_id)
                reason = u'验证成功'
            else :
                self.set_status(status_code=500, reason=u'服务器错误/验证失败')
                reason = u'服务器错误/验证失败'
        except Exception, e:
            self.set_status(status_code=500, reason=u'服务器错误/验证失败')
            reason = e
        self.finish(reason)




# if __name__ == '__main__':
#     sign="PcU0SMJhbPObiIVinNnalZOjI02koWozxLrxa3WQW3rK/n7I+EuVGuXvhsq2MIfUaNiHZDgRFYybGtKr1uuFzEXjA4PwmnDHfWgwRPdjgseoU0eke6ZqGpklBRVTbF6PUy6/vAqur4xb7h1wpdrteUpCPafzDmVPsQLicdojJ/TF9ACjQW8gTNiS6tE9gL5hxy0RJ3/okRJo6dz2pvJBWkjCrgp/r98z/LQijA1o//atZrH63+DcL/GwEOgaymqbodzusXF+g6WMJ/GTJgjdPRHvpO9UAAUKkOQqvwthJvsXIH/L1xqvy+tFpo2J0Ptwg85bowKoyy1qC5ak3sqWqw=="
#     data = open('../SDK/message.json').read()
#     print pp.VerifySign(sign, data)
